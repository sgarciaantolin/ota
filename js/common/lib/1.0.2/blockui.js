/**
 * UDA - DonostiaTIK
 * 2018
 * EUROHELP CONSULTING SL
 */

/**
 * Clase SINGLETON para utilizar la libreria $.blockUI
 */
BlockUI = (function() {

  var my = {};

  // **************
  // *** PUBLIC ***
  // **************

  /**
   * Muestra el blockUi
   *
   * @param data.html: HTML a aplicar al blockUI
   * @param data.message: Mensaje a mostrar en el Spinner
   * @param data.css: Estilos a aplicar al Spinner
   */
  my.show = function(data) {
    // Si no se define 'data', inicializar.
    if (!Utils.isset(data)) {
      data = {};
    }
    // Variables
    var message = data.message || messages.procesando_peticion;
    var html = data.html || '<span class="fa fa-spinner fa-spin"></span><span style="padding-left: 28px;"><strong>' + message + '</strong></span>';
    var css = data.css || {
        backgroundColor: '#000',
        border: 0,
        color: '#fff',
        padding: 15
    };
    // Mostrar blockUi
    $.blockUI({
        message: html,
        css: css
    });
  };

  /**
   * Elimina el blockUi
   */
  my.hide = function() {
    $.unblockUI();
  };

  // **************
  // *** RETURN ***
  // **************

  /**
   * Lista de funciones y variables publicas
   */
  return my;

})();
