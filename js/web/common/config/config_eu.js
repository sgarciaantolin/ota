
/**
 * Configuración en Euskera
 */
var config = {
  code: 2,
  locale: "eus",
  country: "España",
  language: "Euskera",
  phoneFormat: "000 000 000",
  timeFormat: "HH:MM",
  dateFormat: "YYYY/MM/DD",
  datePattern: "^\\d{4}\\/\\d{2}\\/\\d{2}$",
  datetimepicker: {
    format: "YYYY/MM/DD",
    locale: "eu"
  },
  provider: "json", // json, api
};
