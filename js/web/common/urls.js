/**
 * UDA - DonostiaTIK
 * 2018
 * EUROHELP CONSULTING SL
 */

/**
 * Clase SINGLETON para definir las URLs
 */
Urls = (function() {

  var my = {};
  var otaDataDir = "/js/web/ota/data/";
  var commonDataDir = "/js/common/data/";

  // **************
  // *** PUBLIC ***
  // **************

  // --------------------------------------------------------------------------
  // ------------------------------ COMMON ------------------------------------
  // --------------------------------------------------------------------------

  // Callejero : Localidades
  my.COMMON_CALLEJERO_LOCALIDADES               = Routes.url(commonDataDir + "datos_localidades.json"); // FIXME: URL-API

  // Callejero : Calles
  my.COMMON_CALLEJERO_CALLES                    = Routes.url(commonDataDir + "datos_calles.json"); // FIXME: URL-API

  // Callejero : Portales
  my.COMMON_CALLEJERO_PORTALES                  = Routes.url(commonDataDir + "datos_portales.json"); // FIXME: URL-API

  // --------------------------------------------------------------------------
  // -------------------------------- ota -------------------------------------
  // --------------------------------------------------------------------------

  // ota : tabla_actividades.js
  my.ota_ACTIVIDADES                            = Routes.url(otaDataDir + "datos_actividades.json"); // FIXME: URL-API

  // ota : tabla_clubs.js
  my.ota_CLUBS                                  = Routes.url(otaDataDir + "datos_clubs.json"); // FIXME: URL-API

  // ota : tabla_actividades_euskera.js
  my.ota_ACTIVIDADES_EUSKERA                    = Routes.url(otaDataDir + "datos_actividades_euskera.json"); // FIXME: URL-API

  // ota : tabla_propuestas.js
  my.ota_PROPUESTAS                             = Routes.url(otaDataDir + "datos_propuestas.json"); // FIXME: URL-API

  // ota : editor_propuestas.js
  my.ota_PROPUESTAS_RECHAZAR_PROPUESTA          = Routes.url("/api/propuestas/rechazar?id='{0}'");

  // ota : tabla_cursillos.js
  my.ota_CURSILLOS                              = Routes.url(otaDataDir + "datos_cursillos.json"); // FIXME: URL-API

  // ota : tabla_lugares.js
  my.ota_LUGARES                                = Routes.url(otaDataDir + "datos_lugares.json"); // FIXME: URL-API

  // ota : tabla_liquidaciones.js
  my.ota_LIQUIDACIONES                          = Routes.url(otaDataDir + "datos_liquidaciones.json"); // FIXME: URL-API

  // ota : tabla_detalle_liquidaciones.js
  my.ota_DETALLE_LIQUIDACIONES                  = Routes.url(otaDataDir + "datos_detalle_liquidaciones.json"); // FIXME: URL-API

  // ota : tabla_inscripciones.js
  my.ota_INSCRIPCIONES                          = Routes.url(otaDataDir + "datos_inscripciones.json"); // FIXME: URL-API

  // ota : tabla_inscripciones.js
  my.ota_PREINSCRIPCIONES                       = Routes.url(otaDataDir + "datos_preinscripciones.json"); // FIXME: URL-API

  // ota : tabla_inscripciones.js
  my.ota_DEFINICION_CURSILLOS                   = Routes.url(otaDataDir + "datos_definicion_cursillos.json"); // FIXME: URL-API

  // ota : tabla_config_campan.js
  my.ota_CONFIG_CAMPANA                         = Routes.url(otaDataDir + "datos_config_campana.json"); // FIXME: URL-API

  // ota : tabla_detalle_preinscripciones.js
  my.ota_DETALLE_PREINSCRIPCIONES               = Routes.url(otaDataDir + "datos_detalle_preinscripciones.json"); // FIXME: URL-API

  // ota : tabla_sorteo.js
  my.ota_SORTEO                                 = Routes.url(otaDataDir + "datos_sorteo.json"); // FIXME: URL-API

  // ota : tabla_suplementos.js
  my.ota_SUPLEMENTOS                            = Routes.url(otaDataDir + "datos_suplementos.json"); // FIXME: URL-API

 // ota : tabla_memorias.js
  my.ota_MEMORIAS                               = Routes.url(otaDataDir + "datos_memorias.json"); // FIXME: URL-API

 // ota : tabla_memoriasInCursillos.js
  my.ota_MEMORIAS_INS_CURSILLO                   = Routes.url(otaDataDir + "datos_memoriasInsCursillos.json"); // FIXME: URL-API

 // ota : tabla_memoriasPreinsClub.js
  my.ota_MEMORIAS_PREINS_CLUB                   = Routes.url(otaDataDir + "datos_memoriasPreinsClub.json"); // FIXME: URL-API

 // ota : tabla_memoriasPreInsCursillos.js
  my.ota_MEMORIAS_PRE_INS_CURSILLOS             = Routes.url(otaDataDir + "datos_memoriasPreInsCursillos.json"); // FIXME: URL-API

 // ota : tabla_arqueos.js
  my.ota_ARQUEOS                             = Routes.url(otaDataDir + "datos_arqueos.json"); // FIXME: URL-API
  // **************
  // *** RETURN ***
  // **************

  /**
   * Lista de funciones y variables publicas
   */
  return my;

})();
