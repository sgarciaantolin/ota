/**
 * UDA - DonostiaTIK
 * 2018
 * EUROHELP CONSULTING SL
 */

/**
 * Clase SINGLETON para definir metodos de validaciones usando jQuery.Validation.
 */
Validations = (function() {

  var my = {};

  // **************
  // *** PUBLIC ***
  // **************

  /**
   * Metodo de validación que comprueba el formato de un campo
   *
   * @param param: {pattern: "EXPRESION_REGULAR_A_COMPARAR", name: "TIPO_DE_FORMATO_A_INTRODUCIR_POR_EL_USUARIO"}
   */
  my.format = function() {
    // Load method
    jQuery.validator.addMethod("format", function(value, element, param) {

      // Valor opcional, si no esta relleno, correcto.
      if (this.optional(element)) {
        return true;
      }
      // Expresion Regular
      var pattern = new RegExp(param.pattern);
      // Si el valor cumple la expresión regular, correcto, sino error.
      return pattern.test(value);

    }, function(param, element) {
      return validationMessages.format(param.name);
    });
  };

  /**
   * Metodo de validación que comprueba que un campo sea menor o igual que un valor dado.
   *
   * @param param: int/string, "VALOR_A_COMPARAR"
   */
  my.lessEqualTo = function() {
    // Load method
    jQuery.validator.addMethod("lessEqualTo", function(value, element, param) {

      // El valor es optativo, si no esta relleno, correcto.
      if (this.optional(element)) {
        return true;
      }
      // Si se compara un numero
      if (typeof param == 'number') {
        // Si el valor es menor o igual al valor a comparar, correcto.
        if (value <= param) {
          return true;
        }
      }
      // Si se compara una fecha
      else if (typeof param == 'object') { // moment.js
        // Si la fecha es menor o igual al valor a comparar, correcto.
        var date = moment(value, config.dateFormat);
        if (date <= param) {
          return true;
        }
      }
      // Error
      else {
        console.log("Validations:lessEqualTo: Error 'param' desconocido => " + param);
        return false;
      }

    }, function(param, element) {
      if (typeof param == 'number') {
        return validationMessages.lessEqualTo(param);
      } else if (typeof param == 'object') {
        var date = param.format(config.dateFormat);
        return validationMessages.lessEqualTo(date);
      } else {
        console.log("Validations:lessEqualTo: Error 'param' desconocido => " + param);
        return validationMessages.lessEqualTo(/*ERROR*/);
      }
    });
  };

  /**
   * Metodo de validación que comprueba que un campo sea mayor o igual que un valor dado.
   *
   * @param param: {value: "VALOR_A_COMPARAR"}
   */
  my.greaterEqualTo = function() {
    // Load method
    jQuery.validator.addMethod("greaterEqualTo", function(value, element, param) {

      // El valor es optativo, si no esta relleno, correcto.
      if (this.optional(element)) {
        return true;
      }
      // Si se compara un numero
      if (typeof param == 'number') {
        // Si el valor es mayor o igual al valor a comparar, correcto.
        if (value >= param) {
          return true;
        }
      }
      // Si se compara una fecha
      else if (typeof param == 'object') { // moment.js
        // Si la fecha es mayor o igual al valor a comparar, correcto.
        var date = moment(value, config.dateFormat);
        if (date >= param) {
          return true;
        }
      }
      // Error
      else {
        console.log("Validations:greaterEqualTo: Error 'param' desconocido => " + param);
        return false;
      }

    }, function(param, element) {
      if (typeof param == 'number') {
        return validationMessages.greaterEqualTo(param);
      } else if (typeof param == 'object') {
        var date = param.format(config.dateFormat);
        return validationMessages.greaterEqualTo(date);
      } else {
        console.log("Validations:greaterEqualTo: Error 'param' desconocido => " + param);
        return validationMessages.greaterEqualTo(/*ERROR*/);
      }
    });
  };

  /**
   * Metodo de validación que comprueba que un campo sea menor o igual que el de otro campo.
   *
   * @param param: {fieldId: "CAMPO_ID", fieldName: "CAMPO_NOMBRE"}
   */
  my.lessEqualToField = function() {
    // Load method
    jQuery.validator.addMethod("lessEqualToField", function(value, element, param) {

      // Variables
      var fieldValue = $(param.fieldId).val();
      console.log("lessEqualToField: value:" + value + "\nfieldValue: " + fieldValue);
      // Si alguno de los valores no esta definido, correcto.
      if ( !Utils.isset(value) || !Utils.isset(fieldValue) ) {
        return true;
      }
      // Si el valor es mayor al cual se compara, error.
      if (value > fieldValue) {
        return false;
      }
      // Sino, correcto.
      return true;

    }, function(param, element) {
      return validationMessages.lessEqualToField(param.fieldName);
    });
  };

  /**
   * Metodo de validación que comprueba que un campo sea mayor o igual que el de otro campo.
   *
   * @param param: {fieldId: "CAMPO_ID", fieldName: "CAMPO_NOMBRE"}
   */
  my.greaterEqualToField = function() {
    // Load method
    jQuery.validator.addMethod("greaterEqualToField", function(value, element, param) {

      // Variables
      var fieldValue = $(param.fieldId).val();
      // Si alguno de los valores no esta definido, correcto.
      if ( !Utils.isset(value) || !Utils.isset(fieldValue) ) {
        return true;
      }
      // Si el valor es menor al cual se compara, error.
      if (value < fieldValue) {
        return false;
      }
      // Sino, correcto.
      return true;

    }, function(param, element) {
      return validationMessages.greaterEqualToField(param.fieldName);
    });
  };

  /**
   *  ...
   */
  my.emailList = function() {
    jQuery.validator.addMethod("emailList", function(value, element) {

      if (this.optional(element)) return true; // returntru on optional eleent
      var i;
      var valid = true;
      emails = value.split(/[\s,]+/);
      for (i in emails) {
        value = emails[i];
        valid = valid && jQuery.validator.methods.email.call(this,value,element);
      }
      return valid;
    }, 'Uno o mas correos son invalidos');
  };

  /**
   * ...
   */
  my.dateBefore = function() {
    jQuery.validator.addMethod("dateBefore", function (value, element, param) {
         var value2 = $(param.id).val();
         // return new Date(value).getTime() < new Date(value2).getTime();
         return moment(value,config.dateFormat).unix() < moment(value2,config.dateFormat).unix();
    }, function(param, element) {
      return validationMessages.dateBefore(param.at);
    });
  };

  /**
   * ...
   */
  my.dateAfter = function() {
    jQuery.validator.addMethod("dateAfter", function (value, element, param ) {
         var value2 = $(param.id).val();
         // return new Date(value).getTime() > new Date(value2).getTime();
         return moment(value,config.dateFormat).unix() > moment(value2,config.dateFormat).unix();
    },function(param, element) {
      return validationMessages.dateAfter(param.at);
    });
  };

  /**
   * ...
   */
  my.dateBeforeEqual = function() {
    jQuery.validator.addMethod("dateBeforeEqual", function (value, element, param ) {
         var value2 = $(param.id).val();
         // return new Date(value).getTime() <= new Date(value2).getTime();
         return moment(value,config.dateFormat).unix() <= moment(value2,config.dateFormat).unix();
    },function(param, element) {
      return validationMessages.dateBeforeEqual(param.at);
    });
  };

  /**
   * ...
   */
  my.dateAfterEqual = function() {
    jQuery.validator.addMethod("dateAfterEqual", function (value, element, param ) {
         var value2 = $(param.id).val();
         // return new Date(value).getTime() >= new Date(value2).getTime();
         return moment(value,config.dateFormat).unix() >= moment(value2,config.dateFormat).unix();
    },function(param, element) {
      return validationMessages.dateAfterEqual(param.at);
    });
  };

  // **************
  // *** RETURN ***
  // **************

  /**
   * Lista de funciones y variables publicas
   */
  return my;

})();
