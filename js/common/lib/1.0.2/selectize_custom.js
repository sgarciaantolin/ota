/**
 * UDA - DonostiaTIK
 * 2018
 * EUROHELP CONSULTING SL
 */

/**
 * Clase SINGLETON para la gestion de selectizes
 */
SelectizeCustom = function() {

  /**
   * https://selectize.github.io/selectize.js/
   * http://www.jqueryscript.net/demo/jQuery-Plugin-For-Custom-Tags-Input-Select-Box-selectize-js/examples/
   */

  /**
   * Metodo para inicializar un selectize simple
   * Recibe un elemento
   */
  var normal = function(elem) {
    var $select = $(elem.name).selectize(elem.properties);
    control = $select[0].selectize;
    return control;
  };

  /**
   * ...
   */
  var normalClear = function(selectize_elem) {
    return selectize_elem.clear();
  };

  /**
   * Metodo para inicializar dos selectizes dependientes entre ellos
   * El segundo elemento se carga mediante una llamada AJAX
   * Recibe dos elementos JSON
   */
  var combine = function(elem1, elem2) {

    var elem2Call = function(select_elem2, callback) {
      data = {
        "id": $select_elem1[0].value
      };
      data = null;
      successFn = function(response) {
        select_elem2.enable();
        callback(response.data);
      };
      errorFn = function(response) {
        callback();
      };

      Common.requestWS({
        url: elem2.wsMethod,
        data: data,
        "successFn": successFn,
        "errorFn": errorFn
      });
    };

    elem1.properties.onChange = function(value) {
      if (!value.length) return;
      select_elem2.disable();
      select_elem2.clearOptions();
      select_elem2.load(function(callback) {
        elem2Call(select_elem2, callback);
      });
    };

    $select_elem1 = $('#' + elem1.name).selectize(elem1.properties);
    $select_elem2 = $('#' + elem2.name).selectize(elem2.properties);

    select_elem1 = $select_elem1[0].selectize;
    select_elem2 = $select_elem2[0].selectize;

    if (!elem2.edit) {
      select_elem2.disable();
    }

  };

  /**
   * ...
   */
  var onChange = function(elem_list, elems, j) {
    elems[j].properties.onChange = function(value) {
      if (!value.length) return;
      for (k = j; k < elems.length - 1; k++) {
        elem_list[k + 1].js_param.disable();
        elem_list[k + 1].js_param.clearOptions();
      }
      elem_list[j + 1].js_param.load(function(callback) {
        elem2Call(elem_list[j].selectize_param, elem_list[j + 1].js_param, callback, elems[j + 1].wsMethod);
      });
    };
  };

  /**
   * Metodo para inicializar varios selectizes dependientes entre ellos
   * Recibe un array de elementos JSON
   */
  var combineMulti = function(elems) {

    var elem_list = [];
    var elem2Call = function(selectize_elem1, select_elem2, callback, wsMethod) {
      data = {
        "id": selectize_elem1[0].value
      };
      successFn = function(response) {
        select_elem2.enable();
        callback(response.data);
      };
      errorFn = function(response) {
        callback();
      };
      Common.requestWS({
        url: wsMethod,
        data: data,
        "successFn": successFn,
        "errorFn": errorFn
      });
    };

    for (i = 0; i < elems.length; i++) {
      if (i < elems.length - 1) {
        onChange(elem_list, elems, i);
      }
    }

    for (i = 0; i < elems.length; i++) {
      elem_list[i] = {};
      var element = '#' + elems[i].name;
      elem_list[i].selectize_param = $(element).selectize(elems[i].properties);
      elem_list[i].js_param = elem_list[i].selectize_param[0].selectize;

      if (i < elems.length - 1) {
        elem_list[i + 1] = {};
        elem_list[i + 1].selectize_param = $('#' + elems[i + 1].name).selectize(elems[i + 1].properties);
        elem_list[i + 1].js_param = elem_list[i + 1].selectize_param[0].selectize;

        if (!elems[i + 1].edit) {
          elem_list[i + 1].js_param.disable();
        }
      }

    }
  };

  /**
   * Lista de funciones/variables accesibles (publicas)
   */
  return {
    normal: normal,
    combine: combine,
    combineMulti: combineMulti
  };
};
