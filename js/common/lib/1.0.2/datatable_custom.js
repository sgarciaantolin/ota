/**
 * UDA - DonostiaTIK
 * 2018
 * EUROHELP CONSULTING SL
 */

/**
 * Clase para la gestion de las datatables
 */
DataTableCustom = function() {

  var my = {};
  var mDataTable;
  var mDataTableData;

  // **************
  // *** PUBLIC ***
  // **************

  /**
   * Metodo para inicializar la clase
   */
  my.init = function(param) {
    if (typeof(param) == "string") {
      mDataTable = $('#' + param).DataTable() || null;
    } else if (typeof(param) == "object") {
      mDataTableData = param;
    }
  };

  /**
   * Metodo para dibujar la tabla
   */
  my.render = function() {

    // Mostrar spinner loading
    $('#spinnerModal').modal({
      backdrop: 'static',
      keyboard: false
    });
    // Borrar tabla si ya estaba cargada (rellena)
    if ($.fn.dataTable.isDataTable('#' + mDataTableData.elementId)) {
      mDataTable = $('#' + mDataTableData.elementId).DataTable();
      // mDataTable.clear().draw();
      mDataTable.destroy();

      // FIXME: 1º vez, la tabla la crea.
      // FIXME: 2º vez, la tabla la destruye, y la vuelve a crear
      // FIXME: Arreglar para que si esta creada, solo actualice los datos.

    }
    // FIXME: Comprobar que esto se este utilizando ...
    $.fn.dataTable.moment(Common.selectedLanguageMomentDateType2);
    // Cargar y rellenar tabla
    mDataTable = $('#' + mDataTableData.elementId).on('init.dt', function(event) {
      $('#spinnerModal').modal('hide');
      if (typeof mDataTableData.finallyFn === "function") {
        mDataTableData.finallyFn();
      }
    }).DataTable(mDataTableData.properties);
    // Cargar detalles de la tabla
    _.forEach(mDataTableData.details, function(value) {
      detail(value.detailFn, value.detailEvent, value.detailElement);
    });
    // Cargar funciones de la tabla
    _.forEach(mDataTableData.funcs, function(value) {
      funcs();
    });
  };

  /**
   * Metodo para otener una fila de la tabla
   */
  my.getRow = function(row) {
    return mDataTable.row(row);
  };

  /**
   * Metodo para otener los datos de una fila de la tabla
   */
  my.getRowData = function(row) {
    return mDataTable.row(row).data();
  };

  /**
   * Metodo para otener todas las filas de la tabla.
   */
  my.getRows = function() {
    return mDataTable.rows();
  };

  /**
   * Metodo para obtener el indice de una fila
   */
  my.getRowIndex = function(elem) {
    return mDataTable.row(elem).index();
  };

  /**
   * Metodo para obtener una fila segun su indice
   */
  my.getRowFromIndex = function(index) {
    return mDataTable.row(index);
  };

  /**
   * Metodo para añadir una fila
   */
  my.addRow = function(row) {
    return mDataTable.row.add(row).draw(false);
  };

  /**
   * Metodo para eliminar una fila
   */
  my.deleteRow = function(row) {
    row.remove().draw();
  };

  /**
   * Metodo para ordenar la tabla por columnas
   */
  my.orderByCol = function(col) {
    mDataTable.order([col, 'asc']);
  };

  /**
   * Metodo para contar numero total de filas
   */
  my.countRows = function() {
    return mDataTable.data().count();
  };

  /**
   * Metodo para seleccionar una fila de la tabla.
   */
  my.selectRow = function(elem) {
    mDataTable.row(elem).select();
  };

  /**
   * Metodo para de-seleccionar una fila de la tabla.
   */
  my.deselectRow = function(elem) {
    mDataTable.row(elem).deselect();
  };

  /**
   * Metodo para seleccionar todas las filas de la tabla.
   */
  my.selectAll = function() {
    mDataTable.rows().select();
  };

  /**
   * Metodo para de-seleccionar todas las filas de la tabla.
   */
  my.deselectAll = function() {
    mDataTable.rows().deselect();
  };

  /**
   * Metodo para obtener todas las filas seleccionadas.
   */
   my.getSelecteds = function() {
    return mDataTable.rows({
      selected: true
    });
  };

  // ***************
  // *** PRIVATE ***
  // ***************

  /**
   * Metodo para la gestion detalle de los elementos de una tabla
   */
  var detail = function(detailFn, detailEvent, detailElement) {

    // Add event listener for opening and closing details
    $('#' + mDataTableData.elementId + ' tbody').on(detailEvent, detailElement, function(event) {

      var tr = $(this).closest('tr');
      var row = mDataTable.row(tr);

      if (row.child.isShown()) {
        // Animación
        $('div.slider', row.child()).slideUp(function () {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
            // Cambiar icono de 'flecha arriba' por 'flecha abajo'
            tr.find('td > span.fa-chevron-circle-up').removeClass("fa-chevron-circle-up").addClass("fa-chevron-circle-down");
            tr.find('td > span.dtik-icon-arrow-above').removeClass("dtik-icon-arrow-above").addClass("dtik-icon-arrow-below");
        });
      } else {
        // HTML a añadir en el child
        var html = '<div class="slider">' + detailFn(row.data()) + '</div>';
        // Open this row
        row.child(html, 'no-padding').show();
        tr.addClass('shown');
        // Cambiar icono de 'flecha abajo' por 'flecha arriba'
        tr.find('td > span.fa-chevron-circle-down').removeClass("fa-chevron-circle-down").addClass("fa-chevron-circle-up");
        tr.find('td > span.dtik-icon-arrow-below').removeClass("dtik-icon-arrow-below").addClass("dtik-icon-arrow-above");
        // Animación
        $('div.slider', row.child()).slideDown();
      }
    });

  };

  // **************
  // *** RETURN ***
  // **************

  /**
   * Lista de funciones y variables publicas
   */
  return my;
};
