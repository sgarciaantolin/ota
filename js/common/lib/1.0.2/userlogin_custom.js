/**
 * UDA - DonostiaTIK
 * 2018
 * EUROHELP CONSULTING SL
 */

/**
 * Clase SINGLETON para la gestion de login
 */
UserLoginCustom = function() {

  messages = validationMessages;
  $.extend($.validator.messages, messages);

  /**
   * Metodo para inicializar el login de usuario-pass y ticket
   */
  var login = function(elem) {
    loginUserPass(elem);
    loginTicket(elem);
  };

  /**
   * Metodo para login de usuario-pass
   * Recibe elemento con id
   * Se definen las normas del JQueryValidate
   */
  var loginUserPass = function(elem) {
    $('#' + elem.idUserPass).validate({
      // debug: true,
      // onsubmit: true,
      // onkeydown: false,
      // onkeyup: false,
      // onfocusin: false,
      // onfocusout: false,
      // onclick: false,
      focusInvalid: false,
      errorClass: "alert alert-danger",
      highlight: function(element, errorClass) {
        // No añadir el errorClass al element
      },
      rules: {
        email: {
          required: true,
          email: true
        },
        userpass: {
          required: true
        }
      }
    });
  };

  /**
   * Metodo para inicializar el login por ticket
   * Recibe elemento con id
   * Se definen las normas del JQueryValidate
   */
  var loginTicket = function(elem) {
    $('#' + elem.idNticket).validate({
      // debug: true,
      // onsubmit: true,
      // onkeydown: false,
      // onkeyup: false,
      // onfocusin: false,
      // onfocusout: false,
      // onclick: false,
      focusInvalid: false,
      errorClass: "alert alert-danger",
      highlight: function(element, errorClass) {
        // No añadir el errorClass al element
      },
      rules: {
        nticket: {
          required: true
        }
      }
    });
  };

  /**
   * Lista de funciones/variables accesibles (publicas)
   */
  return {
    login: login,
    loginUserPass: loginUserPass,
    loginTicket: loginTicket
  };
};
