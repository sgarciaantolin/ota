/**
 * Translated default messages for the jQuery validation plugin.
 * Locale: ES (Spanish; Español)
 */
var validationMessages = {

  // GENERICAS
  required: "Este campo es obligatorio.",
  remote: "Por favor, rellena este campo.",
  email: "Por favor, escribe una dirección de correo válida.",
  url: "Por favor, escribe una URL válida.",
  date: "Por favor, escribe una fecha válida.",
  dateISO: "Por favor, escribe una fecha (ISO) válida.",
  number: "Por favor, escribe un número válido.",
  digits: "Por favor, escribe sólo dígitos.",
  creditcard: "Por favor, escribe un número de tarjeta válido.",
  equalTo: "Por favor, escribe el mismo valor de nuevo.",
  extension: "Por favor, escribe un valor con una extensión aceptada.",
  maxlength: $.validator.format("Por favor, no escribas más de {0} caracteres."),
  minlength: $.validator.format("Por favor, no escribas menos de {0} caracteres."),
  rangelength: $.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
  range: $.validator.format("Por favor, escribe un valor entre {0} y {1}."),
  max: $.validator.format("Por favor, escribe un valor menor o igual a {0}."),
  min: $.validator.format("Por favor, escribe un valor mayor o igual a {0}."),

  // AÑADIDAS
  nifES: "El NIF es inválido.", // DNI
  nieES: "El NIE es inválido.",
  cifES: "El CIF es inválido.",
  require_from_group: "En este grupo es obligatorio: {0} campo(s)",

  // PROPIAS
  format: $.validator.format("Formato: '{0}'."),
  lessEqualTo: $.validator.format("Este campo tiene que ser menor o igual que '{0}'."),
  greaterEqualTo: $.validator.format("Este campo tiene que ser mayor o igual que '{0}'."),
  lessEqualToField: $.validator.format("Este campo tiene que ser menor o igual que el campo '{0}'."),
  greaterEqualToField: $.validator.format("Este campo tiene que ser mayor o igual que el campo '{0}'."),

  dateBefore: $.validator.format("La fecha tiene que ser anterior a '{0}'."),
  dateAfter: $.validator.format("La fecha tiene que ser posterior a '{0}'."),
  dateBeforeEqual: $.validator.format("La fecha tiene que ser anterior o igual a '{0}'."),
  dateAfterEqual: $.validator.format("La fecha tiene que ser posterior o igual a '{0}'.")
};
