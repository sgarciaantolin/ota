/**
 * UDA - DonostiaTIK
 * 2018
 * EUROHELP CONSULTING SL
 */

/**
 * Clase SINGLETON para la creación de ventanas emergentes
 *
 * Necesita Bootstrap v4.0.0
 */
Dialog = (function() {

  var my = {};

  // **************
  // *** PUBLIC ***
  // **************

  /**
   * Ventana emergente para alertar algo
   */
  my.alert = function(title, buttons) {

    if (!buttons) {
      buttons = {};
      buttons.ok = "OK";
    }

    // Crear unique-id para el dialog
    var timestamp = new Date().valueOf();
    var dialogId = "dialog-" + timestamp;
    var dialogLabel = "dialogLabel-" + timestamp;

    // // Dialog
    // var dialog = '<div class="modal fade" id="' + dialogId + '" tabindex="-1" role="dialog" aria-labelledby="' + dialogLabel + '" aria-hidden="true"> \
    //     <div class="modal-header"> \
    //       <h5 class="modal-title" id="' + dialogLabel + '">' + title + '</h5> \
    //     </div> \
    //     <div class="modal-footer"> \
    //       <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button> \
    //     </div> \
    //   </div>';

    // Dialog
    var dialog = '<div class="modal fade" id="' + dialogId + '" tabindex="-1" role="dialog" aria-labelledby="' + dialogLabel + '" aria-hidden="true"> \
          <div class="modal-dialog" role="document"> \
             <div class="modal-content"> \
                <div class="modal-header"> \
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"> \
                     <span aria-hidden="true">&times;</span> \
                  </button> \
                </div> \
                <div class="modal-body"> \
                  <p class="h4 mb-0 text-primary">' + title + '</p> \
                </div> \
              </div> \
           </div> \
         </div>';

    // Insertar Dialog dentro de la etiqueta
    $('main.EH').append(dialog);
    // Evento para eliminar el Dialog cuando se haya resuelto.
    $('#' + dialogId).on("hidden.bs.modal", function() {
      // Eliminar Dialog
      $('#' + dialogId).remove();
    });
    // Mostrar Dialog
    $('#' + dialogId).modal('show');
  };

  /**
   * Ventana emergente para confirmar alguna acción
   */
  my.confirm = function(title, message, buttons, callback) {

    if (!buttons) {
      buttons = {};
      buttons.ok = "OK";
      buttons.cancel = messages.cancelar;
    }

    // Crear unique-id para el dialog
    var timestamp = new Date().valueOf();
    var dialogId = "dialog-" + timestamp;
    var dialogLabel = "dialogLabel-" + timestamp;
    var dialogAccept = "dialogAccept-" + timestamp;

    // Modal
    var dialog = '<div class="modal fade" id="' + dialogId + '" tabindex="-1" role="dialog" aria-labelledby="' + dialogLabel + '" aria-hidden="true"> \
          <div class="modal-dialog" role="document"> \
             <div class="modal-content"> \
                <div class="modal-header"> \
                   <h5 class="modal-title" id="' + dialogLabel + '">' + title + '</h5> \
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"> \
                      <span aria-hidden="true">&times;</span> \
                   </button> \
                </div> \
                <div class="modal-body"> \
                  <p class="h4 mb-0 text-primary">' + message + '</p> \
                </div> \
                <div class="modal-footer ml-4 mr-4 pl-0 pr-0"> \
                   <button type="button" class="btn btn-light" data-dismiss="modal">' + buttons.cancel + '</button> \
                   <button id="' + dialogAccept + '" type="button" class="btn btn-primary">' + buttons.ok + '</button> \
                </div> \
              </div> \
           </div> \
         </div>';

    // Insertar Dialog dentro de la etiqueta
    $('#portlet').append(dialog);

    // Si se acepta, ejecutar callback
    $('#' + dialogAccept).on('click', function(event) {
      // Esconder el Dialog
      $('#' + dialogId).modal('hide');
      // Lanzar Callback
      if (typeof(callback) == 'function') {
        callback();
      }
    });

    // Evento para eliminar el Dialog cuando se haya resuelto.
    $('#' + dialogId).on("hidden.bs.modal", function() {
      // Eliminar Dialog
      $(this).remove();
    });

    // Mostrar Dialog
    $('#' + dialogId).modal('show');
  };

  /**
   * Ventana emergente para cargar una sub-ventana dentro.
   */
  my.window = function(title, url, callback) {
    // Crear unique-id para el dialog
    var timestamp = new Date().valueOf();
    var dialogId = "dialog-" + timestamp;
    var dialogLabel = "dialogLabel-" + timestamp;
    // Dialog
    var dialog = '<div id="' + dialogId + '" class="modal modal-lg fade" tabindex="-1" role="dialog" aria-labelledby="' + dialogLabel + '" aria-hidden="true"> \
        <div class="modal-header"> \
          <h5 class="modal-title" id="' + dialogLabel + '">' + title + '</h5> \
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"> \
            <span aria-hidden="true">&times;</span> \
          </button> \
        </div> \
        <div id="dialogBody" class="modal-body"> \
          <div class="clear"> \
            <div class="col-md-6"></div> \
            <span class="fa fa-spinner fa-spin"></span> \
            <div class="col-md-6"></div> \
          </div> \
        </div> \
      </div>';
    // Insertar Dialog dentro de la etiqueta
    $('#portlet').append(dialog);
    // Evento para eliminar el Dialog cuando se haya resuelto.
    $('#' + dialogId).on("hidden.bs.modal", function() {
      // Eliminar Dialog
      $('#' + dialogId).remove();
      // Ejecutar callback
      callback();
    });
    // Mostrar Dialog
    $('#' + dialogId).modal('show');
    // Cargar una pagina dinamicamente en el dialog.
    $.get(url, function(view) {
      $("#dialogBody").html(view);
    });
  };

  // **************
  // *** RETURN ***
  // **************

  /**
   * Lista de funciones y variables publicas
   */
  return my;

})();
