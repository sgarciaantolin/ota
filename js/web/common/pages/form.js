/**
 * UDA - DonostiaTIK
 * 2018
 * EUROHELP CONSULTING SL
 */

/**
 * Clase ABSTRACTA y padre de todos los formularios.
 */
Form = function() {

  var my = {};

  // *****************
  // *** ABSTRSACT ***
  // *****************

  // Variables
  my.url = null;

  // Filtro
  my.filterId = null;
  my.$filter = null;
  my.filterValidate = null;

  // Formulario
  my.formId = null;
  my.$form = null;
  my.formValidate = null;

  // Metodos
  my.fill = function() {};
  my.getFilterValidations = function() {};
  my.getFormValidations = function() {};

  // **************
  // *** PUBLIC ***
  // **************

  /**
   * Metodo para inicializar la clase
   */
  my.init = function() {
    initValidations();
  };

  /**
   * Render del formulario
   */
  my.render = function(data) {
    // Llamada ajax
    Common.requestWS({
      url: my.url,
      data: data,
      successFn: function(response) {
        // Rellenar formulario
        my.fill(response.data);
      }
    });
  };

  /**
   * Metodo para filtrar el formulario
   */
  my.filter = function() {
    // Obtener los campos del filtro
    var data = my.$filter.serializeObject(); // data = {"name": "NOMBRE", "value": "VALOR"}
    // Render del formulario
    my.render(data);
  };

  // ***************
  // *** Private ***
  // ***************

  /**
   * Metodo para inicializar las validaciones del filtro.
   */
  var initValidations = function() {

    // Variables
    var key;

    // Carga los mensajes de validación
    $.extend($.validator.messages, validationMessages);

    // Obtener la config de validaciones por defecto
    var validationsDefault = getValidationsDefault();

    // FILTER
    // ---------------------------------------------------------------------

    // Obtener las reglas de validacion aplicadas por el hijo : Filter
    var filterValidationsAux = my.getFilterValidations();

    // Si hay validaciones, validar : Filter
    if (filterValidationsAux) {

      // Fusionar 'filterValidationsAux' con 'validationsDefault' sobre-escribiendo los  parametros si es que los hubiese.
      var filterValidations = validationsDefault;
      for (key in filterValidationsAux) {
        filterValidations[key] = filterValidationsAux[key];
      }

      // Asignamos las validaciones : Filter
      my.filterValidate = my.$filter.validate(filterValidations);

    }

    // FORM
    // ---------------------------------------------------------------------

    // Obtener las reglas de validacion aplicadas por el hijo : Form
    var formValidationsAux = my.getFormValidations();

    // Si hay validaciones, validar : Form
    if (formValidationsAux) {

      // Fusionar 'formValidationsAux' con 'validationsDefault' sobre-escribiendo los  parametros si es que los hubiese.
      var formValidations = validationsDefault;
      for (key in formValidationsAux) {
        formValidations[key] = formValidationsAux[key];
      }

      // Asignamos las validaciones : Form
      my.formValidate = my.$form.validate(formValidations);

    }
  };

  /**
   * Metodo para definir la configuracion por defecto de las validaciones
   */
  var getValidationsDefault = function() {

    return {
      // debug: true,
      // onsubmit: true,
      // onkeydown: false,
      // onkeyup: false,
      // onfocusin: false,
      // onfocusout: false,
      // onclick: false,
      focusInvalid: false,
      errorClass: "is-invalid",
      normalizer: function(value) {
        // Elimnar espacios en blanco antes de validar
        return $.trim(value);
      },
      errorPlacement: function(error, element) {
        // Añadir descripción del error. (tooltip)
        $(element).tooltip({
          placement: "bottom",
          title: $(error).text()
        });
      },
      unhighlight: function(element, errorClass, validClass) {
        var $element = $(element);
        // Eliminar clase de error al 'element'
        $element.removeClass(errorClass);
        // Eliminar descripción del error (tooltip)
        $element.tooltip('dispose');
      }
    };
  };

  // **************
  // *** RETURN ***
  // **************

  /**
   * Lista de funciones y variables publicas
   */
  return my;

};

// **************
// *** STATIC ***
// **************

/**
 * Metodo generico para filtrar un formulario.
 */
Form.reset = function(item) {
  // Obtener id del filtro o formulario
  var formId = $(item).closest('form').attr('id');
  // Reiniciar las validaciones
  $('#' + formId).validate().resetForm();
  // Esconder formulario
  $('#form_content').addClass('hidden');
};

/**
 * Metodo generico para filtrar un formulario.
 */
Form.filter = function(item) {
  // Obtener la clase del filtro
  var clazz = $(item).data('clazz');
  clazz = window[clazz];
  // Validar filtro
  var valid = clazz.$filter.valid();
  // Si es valido, filtrar.
  if (valid) {
    // Filtrar
    clazz.filter();
    // Mostrar formulario
    $('#form_content').removeClass('hidden');
  }
};

// **************
// *** jQuery ***
// **************

/**
 * Event handler para cargar el script una vez recibido el evento "loadForm", El
 * evento "loadForm" salta cuando se termina de cargar toda la pagina.
 */
$(document).on("loadForm", function() {

  console.log('Cargado: form.js');

  /**
   * Evento para el boton 'Limpiar' del filtro
   */
  $('.btnFormReset').on('click', function(event) {
    // Reset
    Form.reset(this);
  });

  /**
   * Evento para el boton 'Filtrar' del filtro
   */
  $('.btnFormFilter').on('click', function(event) {
    // Filtrar
    Form.filter(this);
  });

});
