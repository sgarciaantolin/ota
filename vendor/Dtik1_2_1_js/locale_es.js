
/*
@license
dhtmlxScheduler v.4.4.9 Professional Evaluation

This software is covered by DHTMLX Evaluation License. Contact sales@dhtmlx.com to get Commercial or Enterprise license. Usage without proper license is prohibited.

(c) Dinamenta, UAB.
*/
Scheduler.plugin(function(e){e.locale={date:{month_full:["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],month_short:["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"],day_full:["Domingo","Lunes","Martes","MiÃ©rcoles","Jueves","Viernes","SÃ¡bado"],day_short:["Dom","Lun","Mar","MiÃ©","Jue","Vie","SÃ¡b"]},labels:{dhx_cal_today_button:"Hoy",day_tab:"DÃ­a",week_tab:"Semana",month_tab:"Mes",new_event:"Nuevo evento",icon_save:"Guardar",
icon_cancel:"Cancelar",icon_details:"Detalles",icon_edit:"Editar",icon_delete:"Eliminar",confirm_closing:"",confirm_deleting:"El evento se borrarÃ¡ definitivamente, Â¿continuar?",section_description:"DescripciÃ³n",section_time:"PerÃ­odo",full_day:"Todo el dÃ­a",confirm_recurring:"Â¿Desea modificar el conjunto de eventos repetidos?",section_recurring:"Repita el evento",button_recurring:"Impedido",button_recurring_open:"Permitido",button_edit_series:"Editar la serie",button_edit_occurrence:"Editar este evento",
agenda_tab:"DÃ­a",date:"Fecha",description:"DescripciÃ³n",year_tab:"AÃ±o",week_agenda_tab:"DÃ­a",grid_tab:"Reja",drag_to_create:"Drag to create",drag_to_move:"Drag to move",message_ok:"OK",message_cancel:"Cancel",next:"Next",prev:"Previous",year:"Year",month:"Month",day:"Day",hour:"Hour",minute:"Minute"}}});