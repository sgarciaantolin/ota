/**
 * UDA - DonostiaTIK
 * 2018
 * EUROHELP CONSULTING SL
 */

/**
 * Clase SINGLETON
 *
 * Tabla: Preinscripciones
 */
TablaPreinscripciones = (function() {

  var my = {};
  var mUrl = Urls.ota_PREINSCRIPCIONES;

  // TODO: Mirar porque esta Tabla no es Responsive!!

  my.tableId = "table_listado_preinscripciones";
  my.formId = "form_filtro_preinscripciones";
  my.table =  {
      "info": false, // Mostrar numero de registros
      "responsive": true, // Responsive
      "deferRender": true, // Carga solo las filas a mostrar
      "dom": '<"top"i> rt <"pagination-bottom" p>',
      "columnDefs": [{
          targets: [4,9],
          render: $.fn.dataTable.render.ellipsis("10")
      }],
      "columns": [
        {
          "data": "grupo",
          "render": function(data, type, full, meta) {
            var icono = 'dtik-icon-user-' + data;
            return'<span class="' + icono + '"></span>';
          }
        },
        {
          "data": "dni",
          "render": function(data, type, full, meta) {
            return '<a href="detalle_preinscripcion.html">' + data +'</a>';
          }
        },
        { "data": "nombre" },
        { "data": "edad"},
        { "data": "club" },
        { "data": "actividad" },
        {
          "data": "fechaInicioCursillo",
          "className": "text-right",
          "render": function(data, type, full, meta) {
            // Convertir timestamp a fecha
            return moment.unix(data).format(config.dateFormat);
          }
        },
        // { "data": "frecuencia" },
        { "data": "horario" },
        { "data": "idioma" },
        { "data": "lugar" },
        {
          "data": "estadoId",
          "className": "text-right",
          "render": function(data, type, full, meta) {
            var estadoCSS = Constantes.estados[data].css;
            var estadoNombre = Constantes.estados[data].name[4];
            return '<span class="badge ' + estadoCSS + '">' + estadoNombre + '</span>';
          }
        }
      ],
      "order": [
        // [2, 'asc'] // ordenar por tercera columna
      ],
      "language": dataTableMessages
  };
  my.rulesConf =  {
      rules: {
        "filtroActividad": {
          required: {
            depends: function(element) {
              return !$('#filtroActividad').prop('disabled');
            }
          }
        },
        "filtroClub": {
          required: {
            depends: function(element) {
              return !$('#filtroClub').prop('disabled');
            }
          }
        }
      }
  };

  // **************
  // *** PUBLIC ***
  // **************

  my.loadTable = function(dataSet) {
          var ts = '#' + my.tableId; // ts stands for tableSelector
          my.table.data = dataSet;
          if (dataSet.length === 0) {return;}
          if ($.fn.dataTable.isDataTable(ts)) {
           $(ts).DataTable().clear();
           $(ts).DataTable().rows.add(dataSet).draw();
         } else $(ts).DataTable(my.table).draw();
         $(ts).show();
         $('#table_content').show();
  };

  my.limpiar = function(tableId) {
                  var ts = '#' + my.tableId; // ts stands for tableSelector
                  if ($.fn.dataTable.isDataTable(ts)) {
                     $(ts).DataTable().clear().draw();
                     $(ts).hide();
                     $('#table_content').hide();
                   }
  };

  // **************
  // *** RETURN ***
  // **************

  /**
   * Lista de funciones y variables publicas
   */
  return my;

})();

/**
 * Event handler para cargar el script una vez recibido el evento "domLoaded",
 * El evento "domLoaded" salta cuando se termina de cargar toda la pagina.
 */
$(document).on("domLoaded", function() {

  var ts = '#' + TablaPreinscripciones.tableId;
  var fs = '#' + TablaPreinscripciones.formId;
  // Si no estamos en la pagina del script, salir.
  if ($(ts).length == 0) { return; }
  console.log('Cargado: /ota/tabla_preinscripciones.js');


  var buscadorPreinscripciones = new Buscador(TablaPreinscripciones.formId, Urls.ota_PREINSCRIPCIONES,
                                              TablaPreinscripciones.rulesConf);
 $(ts).hide();
  // **************
  // *** jQuery ***
  // **************

  // Deshabilitar el 'ID' si se filtra por algun <SELECT>
  $('#filtroActividad, #filtroClub').on('change', function(event) {
    $('#filtroDNI').prop('disabled', true);
    $('#filtroName').prop('disabled', true);
  });

  // Deshabilitar los <SELECT> si se filtra por 'ID'
  $('#filtroDNI, #filtroName').on('input', function(event) {
    // Reiniciar las validaciones
    var validator = $(fs).validate().resetForm();
    // Deshabilitar <SELECT>
    $('#filtroActividad').prop('disabled', true);
    $('#filtroClub').prop('disabled', true);
  });

  $('.dropdown-item').on('click', function(event) {
    var buttons = {
       ok: "Continuar",
       cancel: "Cancelar"
    };
    if (this === $(this).parent().children()[1])
      // show message
     Dialog.BootstrapDialogConfirm(messages.exportari_csv, messages.exportar_csv_preinscripciones, buttons, function() {console.log("sending to the server");});
     // TODO: falta sustituir el null con la funcion que realizara la operacion de enviar
      //Dialog.confirm("Exportar a CSV", messages.exportar_csv, function () {console.log("hay");});
  });


  // Boton 'Limpiar' del formulario
  $('#btnReset').on('click', function(event) {
    TablaPreinscripciones.limpiar();
    buscadorPreinscripciones.limpiar();
  });

  // Boton 'Buscar' del formulario
  $('#btnFilter').on('click', function(event) {
    // Si NO se busca por 'ID' des-habilitar el campo.
    event.preventDefault();
    buscadorPreinscripciones.filtrar();
  });

  // $(document).on('endEvent', TablaCursillos.loadTable);

  $(fs).on('endFilter', function (event, receivedData) {
    TablaPreinscripciones.loadTable(receivedData);
  });

  // Activar Tootlips
  MyJQuery.tooltip();

});
