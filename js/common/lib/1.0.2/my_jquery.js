/**
 * UDA - DonostiaTIK
 * 2018
 * EUROHELP CONSULTING SL
 */

/**
 * Clase SINGLETON que recoge metodos jQuery GENERICOS.
 *
 * Librerias necesarias
 * - TimePicker
 * - DatePicker
 *
 */
MyJQuery = (function() {

  var my = {};

  // **************
  // *** PUBLIC ***
  // **************

  /**
   * TimePicker
   */
  my.timepicker = function() {

    $('.mytimepicker').each(function(key, value) {
      // Si es solo un input-timepicker
      if ($(value).is("input")) {
        $('.mytimepicker').timepicker({});
      }
      // Si es un component-timepicker
      else {
        $(value).find('input').timepicker({
          timeFormat: 'HH:mm',
          dynamic: false,
          dropdown: true,
          scrollbar: true
        });
        $(value).on('click', 'div', function(event) {
          event.stopPropagation();
          $(value).find('input').focus();
        });
      }
    });

    // Añadir evento (timepicker) al icono
    $('body').on('click', '.mytimepicker div', function(event) {
      event.stopPropagation();
      $(event.currentTarget).find('input').focus();
    });
  };

  /**
   * DatePicker
   */
  my.datepicker = function() {

    $.fn.datepicker.dates.XX = config.datepicker;
    $('.mydatepicker').datepicker({
      todayHighlight: true,
      autoclose: true,
      language: 'XX'
    });
  };

  /**
   * DateTimePicker
   */
  my.datetimepicker = function() {
    $('.js-tempusdominus-yyyy-mm-dd-es, .js-tempusdominus-yyyy-mm-dd-eu, .js-tempusdominus-eu, .mydatetimepicker').datetimepicker(
      config.datetimepicker
    );
  };

  /**
   * Tooltip
   */
  my.tooltip = function() {

    $('body').tooltip({
      selector: '[data-toggle="tooltip"]'
    });
  };

  /**
   * Checkboxs para seleccionar una o todas las filas de una tabla
   */
  my.checkboxSelectRows = function() {

    // Selecciona o deselecciona todas las filas de una tabla
    $('table').on('change', 'thead input[type="checkbox"]', function(event) {
      // Obtener tabla
      var table = event.delegateTarget;
      // Inicializar DataTableCustom
      var dataTableCustom = DataTableCustom();
      dataTableCustom.init(table.id);
      // Si el input esta checkeado
      if (this.checked) {
        // Selecciona todos los checks del tbody
        dataTableCustom.selectAll();
        $(table).find('tbody input[type="checkbox"]').prop('checked', true);
      } else {
        // Deselecciona todos los checks del tbody
        dataTableCustom.deselectAll();
        $(table).find('tbody input[type="checkbox"]').prop('checked', false);
      }
    });
    // Selecciona o deselecciona una fila
    $('table').on('change', 'tbody input[type="checkbox"]', function(event) {
      // Obtener tabla
      var table = event.delegateTarget;
      // Obtener fila que se ha checkeado
      var tr = $(this).closest("tr");
      // Inicializar DataTableCustom
      var dataTableCustom = DataTableCustom();
      dataTableCustom.init(table.id);
      // Si el input esta checkeado
      if (this.checked) {
        dataTableCustom.selectRow(tr);
      } else {
        dataTableCustom.deselectRow(tr);
      }
    });
  };

  // **************
  // *** RETURN ***
  // **************

  /**
   * Lista de funciones y variables publicas
   */
  return my;

})();
