
/**
 * Configuración en Castellano
 */
var config = {
  code: 1,
  locale: "es",
  country: "España",
  language: "Español",
  phoneFormat: "000 000 000",
  timeFormat: "HH:MM",
  dateFormat: "DD/MM/YYYY",
  datePattern: "^\\d{2}\\/\\d{2}\\/\\d{4}$",
  datetimepicker: {
    format: "DD/MM/YYYY",
    locale: "es"
  },
  provider: "json", // json, api
};
