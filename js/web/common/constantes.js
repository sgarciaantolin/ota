/**
 * UDA - DonostiaTIK
 * 2018
 * EUROHELP CONSULTING SL
 */

/**
 * Clase SINGLETON
 *
 * Constantes
 */
Constantes = (function() {

  // Variables
  var my = {};

  /**
   * Tipos de Documento
   */
  my.DNI      = 1;
  my.NIE      = 2;
  my.PASSPORT = 3;

  /**
   * Estados
   */
  my.estados = {
    "E0001": {
      "name": {
        1: "Activo"
      },
      "css": "badge-success" // Verde
    },
    "E0002": {
      "name": {
        1: "Pendiente"
      },
      "css": "badge-secondary" // Amarilo
    },
    "E0003": {
      "name": {
        1: "Baja"
      },
      "css": "badge-warning" // Verde
    },
    "E0004": {
      "name": {
        1: "Denegado"
      },
      "css": "badge-danger" // Rojo
    },
    "E0005": {
      "name": {
        1: "En Revocación"
      },
      "css": "badge-info" // Rojo
    },
    "E0006": {
      "name": {
        1: "Revocación"
      },
      "css": "badge-dark" // Rojo
    }
  };

  // **************
  // *** RETURN ***
  // **************

  /**
   * Lista de funciones y variables publicas
   */
  return my;

})();
