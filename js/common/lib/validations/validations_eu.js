/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: EUS (Basque; Euskera)
 */
var validationMessages = {

  // GENERICAS
  required: "Eremu hau beharrezkoa da.",
  remote: "Mesedez, bete eremu hau.",
  email: "Mesedez, idatzi baliozko posta helbide bat.",
  url: "Mesedez, idatzi baliozko URL bat.",
  date: "Mesedez, idatzi baliozko data bat.",
  dateISO: "Mesedez, idatzi baliozko (ISO) data bat.",
  number: "Mesedez, idatzi baliozko zenbaki oso bat.",
  digits: "Mesedez, idatzi digituak soilik.",
  creditcard: "Mesedez, idatzi baliozko txartel zenbaki bat.",
  equalTo: "Mesedez, idatzi berdina berriro ere.",
  extension: "Mesedez, idatzi onartutako luzapena duen balio bat.",
  maxlength: $.validator.format("Mesedez, ez idatzi {0} karaktere baino gehiago."),
  minlength: $.validator.format("Mesedez, ez idatzi {0} karaktere baino gutxiago."),
  rangelength: $.validator.format("Mesedez, idatzi {0} eta {1} karaktere arteko balio bat."),
  range: $.validator.format("Mesedez, idatzi {0} eta {1} arteko balio bat."),
  max: $.validator.format("Mesedez, idatzi {0} edo txikiagoa den balio bat."),
  min: $.validator.format("Mesedez, idatzi {0} edo handiagoa den balio bat."),

  // AÑADIDAS
  nifES: "El NIF es inválido.",  // DNI
  nieES: "El NIE es inválido.",
  cifES: "El CIF es inválido.",
  require_from_group: "En este grupo es obligatorio: {0} campo(s)",

  // PROPIAS
  format: $.validator.format("Formato: '{0}'."),
  lessEqualTo: $.validator.format("Este campo tiene que ser menor o igual que '{0}'."),
  greaterEqualTo: $.validator.format("Este campo tiene que ser mayor o igual que '{0}'."),
  lessEqualToField: $.validator.format("Este campo tiene que ser menor o igual que el campo '{0}'."),
  greaterEqualToField: $.validator.format("Este campo tiene que ser mayor o igual que el campo '{0}'."),

  dateBefore: $.validator.format("La fecha tiene que ser anterior a '{0}'."),
  dateAfter: $.validator.format("La fecha tiene que ser posterior a '{0}'."),
  dateBeforeEqual: $.validator.format("La fecha tiene que ser anterior o igual a '{0}'."),
  dateAfterEqual: $.validator.format("La fecha tiene que ser posterior o igual a '{0}'.")
};
