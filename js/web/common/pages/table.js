/**
 * UDA - DonostiaTIK
 * 2018
 * EUROHELP CONSULTING SL
 */

/**
 * Clase ABSTRACTA y padre de todas las tablas.
 */
Table = function() {

  var my = {};
  var mData = null;

  // *****************
  // *** ABSTRSACT ***
  // *****************

  // Variables
  my.url = null;
  my.dataTableCustom = null;

  // Filtro
  my.filterId = null;
  my.$filter = null;
  my.filterValidate = null;

  // Tabla
  my.tableId = null;
  my.$table = null;

  // Tabla : Acciones
  my.urlNewItem = null;
  my.urlEditItem = null;
  my.urlDeleteItem = null;

  // Metodos
  my.getValidations = function() {};
  my.getDataTableData = function() {};

  // **************
  // *** PUBLIC ***
  // **************

  /**
   * Metodo para inicializar la clase
   */
  my.init = function() {
    initValidations();
    initDataTableCustom();
  };

  /**
   * Metodo para dibujar la tabla
   */
  my.render = function() {
    // Render de la tabla
    my.dataTableCustom.render();
    // Mostrar tabla
    $('#table_content').removeClass('hidden');
  };

  /**
   * Metodo para recargar la tabla
   */
  my.reload = function() {
    // Reload de la tabla
    my.dataTableCustom.reload();
  };

  /**
   * Metodo para duplica un elemento de la tabla
   */
  my.duplicateItem = function(rowId) {

    // Si no hay 'urlDuplicateItem' o la 'id', return.
    var urlDuplicateItem = my.urlDuplicateItem;
    if (!Utils.isset(urlDuplicateItem) || !Utils.isset(rowId)) {
      return;
    }

    // Llamada ajax
    Common.requestWS({
      url: my.url,
      data: mData,
      // url: my.urlDuplicateItem + "?id=" + rowId, // TODO: Descomentar para
      // versión Spring
      // method: "PUT", // TODO: Descomentar para versión Spring
      successFn: function(response) {
        // TODO: Descomentar para versión Mock
        my.render();
        // // TODO: Descomentar para versión Spring
        // // Comprobar que la id existe, no es nula o vacia ni 0
        // if (Utils.isset(response.id)) {
        // // Recargar DataTable
        // my.render();
        // }
      }
    });
  };

  /**
   * Metodo para elimina un elemento de la tabla
   */
  my.deleteItem = function(rowId) {

    // Si no hay 'urlDeleteItem' o la 'id', return.
    var urlDeleteItem = my.urlDeleteItem;
    if (!Utils.isset(urlDeleteItem) || !Utils.isset(rowId)) {
      return;
    }

    // Llamada ajax
    Common.requestWS({
      url: my.url,
      data: mData,
      // url: my.urlDeleteItem() + "?id=" + rowId, // TODO: Descomentar para
      // versión Spring
      // method: "DELETE", // TODO: Descomentar para versión Spring
      successFn: function(response) {
        // TODO: Descomentar para versión Mock
        my.render();
        // // TODO: Descomentar para versión Spring
        // // Numero de filas afectadas por el delete
        // var rows = response.rowsAffected;
        // // Comprobar numero de filas afectadas
        // if (parseInt(rows) > 0) {
        // // Recargar DataTable
        // my.render();
        // }
      }
    });
  };

  // ***************
  // *** Private ***
  // ***************

  /**
   * Metodo para inicializar las validaciones del filtro.
   */
  var initValidations = function() {

    // Obtener las reglas de validacion aplicadas por el hijo
    var validationsAux = my.getValidations();

    // Si no hay validaciones, return
    if (!validationsAux) {
      return;
    }

    // Obtener la config de validaciones por defecto
    var validationsDefault = getValidationsDefault();

    // Fusionar 'validationsAux' con 'validationsDefault' sobre-escribiendo los
    // parametros si es que los hubiese.
    var validations = validationsDefault;
    validations.rules = validationsAux.rules;

    // Carga los mensajes de validación
    $.extend($.validator.messages, validationMessages);

    // Asignamos las validaciones al formulario filtro
    my.filterValidate = my.$filter.validate(validations);
  };

  /**
   * Metodo para inicializar el dataTableCustom.
   */
  var initDataTableCustom = function() {

    // Obtener la config por defecto del dataTableData.
    var dataTableDataDefault = getDataTableDataDefault();

    // Obtener config del dataTableData definido por el hijo
    var dataTableDataAux = my.getDataTableData();

    // Fusionar 'dataTableDataAux' con 'configDataTableDataDefault'
    // sobre-escribiendo los parametros si es que los hubiese.
    var dataTableData = dataTableDataDefault;
    for (var key in dataTableDataAux.properties) {
      dataTableData.properties.key = dataTableDataAux.properties.key;
    }
    if (dataTableDataAux.details) {
      dataTableData.details = dataTableDataAux.details;
    }

    // Inicializamos el dataTableCustom
    my.dataTableCustom = new DataTableCustom();
    my.dataTableCustom.init(dataTableData);
  };

  /**
   * Metodo para definir la configuracion por defecto de las validationes.
   */
  var getValidationsDefault = function() {

    return {
      // debug: true,
      // onsubmit: true,
      // onkeydown: false,
      // onkeyup: false,
      // onfocusin: false,
      // onfocusout: false,
      // onclick: false,
      focusInvalid: false,
      errorClass: "is-invalid",
      normalizer: function(value) {
        // Elimnar espacios en blanco antes de validar
        return $.trim(value);
      },
      errorPlacement: function(error, element) {
        // Añadir descripción del error. (tooltip)
        $(element).tooltip({
          placement: "bottom",
          title: $(error).text()
        });
      },
      unhighlight: function(element, errorClass, validClass) {
        var $element = $(element);
        // Eliminar clase de error al 'element'
        $element.removeClass(errorClass);
        // Eliminar descripción del error (tooltip)
        $element.tooltip('dispose');
      }
    };
  };

  /**
   * Metodo para definir la configuracion por defecto del dataTableData.
   */
  var getDataTableDataDefault = function() {

    // Variable
    var dataTableData = {};

    // Rellenar
    dataTableData.elementId = my.tableId;
    dataTableData.properties = {
      // "data" : null, // Datos de la tabla
      "ajax": {
        "url": my.url,
        "data": function(d) {
          // Obtener datos del filtro
          data = my.$filter.serializeObject(); // data = {"campo1": "valor1",
          // "campo1": "valor1", ...}
          // Añadir al objeto 'd' los parametros del filtro
          for (var key in data) {
            d[key] = data[key];
          }
        },
        "error": function(request, status, error) {
          console.log("Ajax: ErrorCode [" + request.status + "] => [" + request.responseJSON + "]");
        }
      },
      "info": false, // Mostrar info de la tabla: "1 de 10 of 100 entidades"
      "select": {
        style: 'single'
      }, // Poder seleccionar una fila
      // "filter" : false,
      // "paging" : false,
      // "ordering" : false, // Habilitar ordenar columnas
      // "searching" : false, // Campo para buscar en la tabla
      "responsive": true, // Responsive
      // "pageLength" : 10,
      "processing": true,
      "serverSide": true,
      "deferRender": true, // Solo renderiza el numero de filas segun
      // paginacion
      // "deferLoading" : [10, 0], // No hacer llamada ajax hasta que se haga:
      // draw(), ajax.load(), ajax.reload()...
      // "lengthChange" : false, // Mostrar numero de registros por pagina
      "footerCallback": function(row, data, start, end, display) {
        // Variables
        var api = this.api();
        var $tfoot = my.$table.find('tfoot');
        // Si no hay datos o no hay <tfoot> ...
        if (!data.length || !$tfoot.length) {
          // Esconder <tfoot>
          $tfoot.addClass('hidden');
          // Exit
          return;
        }
        // Por cada fila ...
        api.columns().every(function() {
          // Variables
          var columna = this, value = "";
          // Si la columna tiene la clase 'sumTotal'
          if ($(columna.footer()).hasClass('sumTotal')) {
            // Calcular su total
            value = columna.data().reduce(function(a, b) {
              var x = parseFloat(a) || 0;
              var y = parseFloat(b) || 0;
              return x + y;
            }, 0);
          }
          // A futuro añadir aqui mulTotal, printTotal, o lo que quieras ...
          // ...
          // Asignar valor a la columna
          if (value) {
            var index = columna[0];
            var column = api.column(index).footer();
            $(column).html(value);
          }
          // Mostrar <tfoot>
          $tfoot.removeClass('hidden');
        });
      },
      "dom": '<"top"i> rt <"pagination-bottom" p>',
      "language": dataTableMessages
    };

    // Return
    return dataTableData;
  };

  // **************
  // *** RETURN ***
  // **************

  /**
   * Lista de funciones y variables publicas
   */
  return my;

};

// **************
// *** STATIC ***
// **************

/**
 * Metodo generico para el botón 'Limpiar' del formulario.
 *
 * @param item: #btnTableReset
 */
Table.reset = function(item) {

  // Obtener la clase de esta tabla
  var clazz = $(item).closest('.EH').data('clazz');
  clazz = window[clazz];

  // Reiniciar las validaciones del filtro
  clazz.filterValidate.resetForm();

  // Obtener atributo 'data-load' en el botón #btnTableReset
  var load = $(item).data('load');

  // Si load esta definido, cargar tabla por defecto (sin filtro)
  if (load) {
    // Esperar a que termine el evento <button type="reset"> del formulario.
    setTimeout(function() {
      // Reload del DataTable
      clazz.reload();
    }, 1);
  }
  // Sino, es que el filtro es obligatorio, esconder la tabla hasta que se filtre.
  else {
    // Esconder tabla
    $('#table_content').addClass('hidden');
  }
};

/**
 * Metodo generico para el botón 'Filtrar' del formulario.
 *
 * @param item: #btnTableFilter
 */
Table.filter = function(item) {

  // Obtener la clase del filtro
  var clazz = $(item).closest('.EH').data('clazz');
  clazz = window[clazz];

  // Validar filtro
  var valid = clazz.$filter.valid();

  // Si es valido, filtrar.
  if (valid) {
    // Filtrar tabla
    clazz.render();
  }

  // Des-habilitar botones de accion la tabla
  $('#btnTableEdit').prop('disabled', true);
  $('#btnTableDelete').prop('disabled', true);
};

// **************
// *** jQuery ***
// **************

/**
 * Event handler para cargar el script una vez recibido el evento "loadTable",
 * El evento "loadTable" salta cuando se termina de cargar toda la pagina.
 */
$(document).on("loadTable", function() {

  console.log('Cargado: table.js');

  /**
   * Evento para simular click en el boton 'Buscar' si es que el formulario hace
   * submit.
   */
  $('form').submit(function(event) {
    // Cancelar redirección
    event.preventDefault();
    // Simular click del boton 'Buscar'
    $("#btnTableFilter").click();
  });

  /**
   * Evento para habilitar y des-habilitar botones de accion (editar,
   * eliminar...)
   */
  $('table').on('click dblclick', 'tbody tr', function(event) {
    // Obtener item clickeado
    var $tr = $(this);
    // Si la tabla esta vacia, return
    if ($tr.find('td.dataTables_empty').length) {
      return;
    }
    // Si se está de-seleccionando
    if ($tr.hasClass('selected')) {
      // Deshabilita botones
      $tr.removeClass('selected');
      $('#btnTableEdit').prop('disabled', true);
      $('#btnTableDelete').prop('disabled', true);
    } else {
      // Habilita botones
      $tr.addClass('selected');
      $('#btnTableEdit').prop('disabled', false);
      $('#btnTableDelete').prop('disabled', false);
    }
  });

  /**
   * Evento para controlar posible error por si se clickea algun boton de acción
   * y no hay fila seleccionada.
   */
  $('#btnTableEdit, #btnTableDelete').on('click', function(event) {
    // Obtener fila selecciona en la tabla
    var $tr = $('tr.selected');
    // Si no existe la fila o hay mas de una, error
    if ($tr.length != 1) {
      // Log
      console.log('table:click: Error, no se ha encontrado una fila seleccionada.');
      // Deshabilitar botón
      $(this).prop('disabled', true).addClass('disabled');
      // Exit
      event.stopPropagation();
      // event.stopImmediatePropagation()
      // return;
    }
  });

  /**
   * Evento para el boton 'Limpiar' del filtro
   *
   * NOTA: Se saca el codigo a una función, porque se puede sobre-escribir el
   * evento.
   */
  $('#btnTableReset').on('click', function(event) {
    // Reset
    Table.reset(this);
  });

  /**
   * Evento para el boton 'Filtrar' del filtro
   *
   * NOTA: Se saca el codigo a una función, porque se puede sobre-escribir el
   * evento.
   */
  $('#btnTableFilter').on('click', function(event) {
    // Filter
    Table.filter(this);
  });

});
