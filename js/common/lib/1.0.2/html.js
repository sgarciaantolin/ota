/**
 * UDA - DonostiaTIK
 * 2018
 * EUROHELP CONSULTING SL
 */

/**
 * Clase SINGLETON para crear HTML dinamico
 */
Html = (function() {

  var my = {};

  // **************
  // *** PUBLIC ***
  // **************

  /**
   * Metodo para crear la estructura HTML de un <select>
   *
   * @param data.attr: Map, atributos a añadir al <select>
   * @param data.default: String, texto de la opción por defecto
   * @param data.selected: String, opcion pre-seleccionada
   * @param data.options: Array/Map, datos a añadir en los option que componene el select
   * @param data.optionsWithvalue: Boolean, añadir value a los option
   */
  my.select = function(data) {

    var key;

    // Select : Open
    var select = '<select';
    for(key in data.attr) {
      select += ' ' + key + '="' + data.attr[key] + '"';
    }
    select += '>\n';

    // Si existe, añadir opcion por defecto.
    var options = '';
    if (Utils.isset(data.default)) {
      options = '<option value="">' + data.default + '</option>\n';
    }

    // Select : Body
    if (data.optionsWithvalue) {
      // Option con value
      for(key in data.options) {
        options += '<option value="' + key + '"';
        if (data.selected == key) {
          options += ' selected';
        }
        options += '>' + data.options[key] + '</option>\n';
      }
    } else {
      // Option sin value
      for(key in data.options) {
        options += '<option';
        if (data.selected == data.options[key]) {
          options += ' selected';
        }
        options += '>' + data.options[key] + '</option>\n';
      }
    }

    // Select : Close
    return select + options + '</select>\n';
  };
  // **************
  // *** RETURN ***
  // **************

  /**
   * Lista de funciones y variables publicas
   */
  return my;

})();
