
/**
 * UDA - DonostiaTIK
 * 2018
 * EUROHELP CONSULTING SL
 */


 /**
  *
  * Clase Padre Buscador
  *
  */
Buscador = function(formId, url, validationConf, callback) {

    this.filter = callback;
    this.received = null; // donde se guardan los datos recividos del servidor despues de una peticion Ajax
    // Variables o atributos privados
    var validator = null; // variable que almacena el objeto validator para este formulario
    var stadoInicial = null; // almacena los datos por defecto del formulario Buscador
    var endFilterEvent = document.createEvent('Event');
    var defaultValidation = {
             errorClass: "is-invalid",
             normalizer: function(value) {
                           // Elimnar espacios en blanco antes de validar
                           return $.trim(value);
             },
             unhighlight: function(element, errorClass, validClass) {
                           var $element = $(element);
                           // Eliminar clase de error al 'element'
                           $element.removeClass(errorClass);
                           // Eliminar descripción del error (tooltip)
                           $element.tooltip('dispose');
             },
             submitHandler: function (form) {
                   var dts = $(form).serializeArray(); // data = {"name": "NOMBRE", "value": "VALOR"}
                   // TODO: enviar y recivir datos del servidor

                   //  EN CASO DE NECESITAR EL METODO GET DE ENVIO DESCOMENTAR EL CODIGO  QUE ESTA A CONTINUACION
                   // newUrl = url;
                   // Si hay filtro, construimos la URL-API con los parametros del filtro.
                   // if (dts.length != 0) {
                   //    // Contruir URL para obtener las propuestas segun el filtro.
                   //    newUrl += "?";
                   //    // Añadir parametros a la URL
                   //    dts.forEach(function(field) {
                   //       newUrl += (field.name + "='" + field.value + "'&");
                   //    });
                   //    // Quitar ultimo '&'
                   //    newUrl = newUrl.substring(0, newUrl.length - 1);
                   // }
                   // console.log("newURL: " + newUrl);
                   $.ajax({
                     url: url,
                     dataType:"json",
                     data: dts,
                     success: function(data) {
                       $.event.trigger("endFilterEvent",[data.data]);
                     }
                   });
             },
             errorPlacement: function(error, element) {
                                 // Añadir descripción del error. (tooltip)
                                 // console.log("errorPlacement");
                                 $(element).tooltip({
                                   placement: "bottom",
                                   title: $(error).text()
                                 });
            },
    };

    // Metodos privados
    var saveInitialState = function (formId) {
        var formSelector = '#' + formId;
        stadoInicial = $(formSelector).serializeArray();
    };

    var updateValidation = function (validationConf) {
        // actualiza el validation conf por defecto con los nueva configuracion
        var i;
        var newValidationConf = defaultValidation;
        if (typeof(validationConf) === "object") {
           for (i in validationConf)
              newValidationConf[i] = validationConf[i];
        } else { // escribe un mensaje de error y deja el de defecto.
            console.error("\nError: La configuracion de la validacion tiene que ser un objeto." +
                          "Se dejara la validacion que esta definida por defecto\n");
        }
        return newValidationConf;
    };

    // Variables o atributos publicos
    this.formId = null; // id del formulario
    this.formSelector = null;
    this.validationConf = null; // guarda la configuracion para la validacion del Buscador
    this.url = null; // la url al que se le va hacer la peticion ajax
    this.filter = null; // Funcion que se utilizara al pulsar el boton de filtrar

     // cuerpo del constructor
     if (!formId || formId === "" ) {
         console.error("Error: no se ha suministrado el id del formulario");
         return;
     }
     if (!url ||  url === "" ) {
        console.error("Error: no se ha suministrado la url");
        return;
     }


     this.validationConf = updateValidation(validationConf);
     this.formId = formId;
     this.formSelector = '#' + this.formId;
     this.url = url;
     saveInitialState(this.formId);
     endFilterEvent.initEvent('endFilter', true, true);
     //registrar el formulario del filtro como subscriptor de este evento
     $(document).on('endFilterEvent', function (event, receivedData) {
       $('#' + formId).trigger('endFilter', [receivedData]);
     });
     // Carga los mensajes de validación
     $.extend($.validator.messages, validationMessages);

     Validations.format();
     Validations.lessEqualTo();
     Validations.greaterEqualTo();
     Validations.lessEqualToField();
     Validations.greaterEqualToField();
     Validations.emailList();
     Validations.dateBefore();
     Validations.dateAfter();
     Validations.dateBeforeEqual();
     Validations.dateAfterEqual();
     validator = $(this.formSelector).validate(this.validationConf);
};

// Metodos publicos
Buscador.prototype.limpiar = function() {
         var formSelector = '#' + this.formId;
         var validator = $(formSelector).validate();
         $('input, select, button').prop("disabled",false);
         if (validator !== null) validator.resetForm();
};

Buscador.prototype.filtrar = function(callbackFun) {
   var formSelector = '#' + this.formId;
   $(formSelector).submit();
};
