/**
 * UDA - DonostiaTIK
 * 2018
 * EUROHELP CONSULTING SL
 */

/**
 * Clase libreria para la gestion de calendarios
 * Se puede inicializar un calendario simple o uno de rango
 * El calendario a inicializar depende de la funcion que se llame y el elem que reciba
 */
DateRangePickerCustom = function() {

  /**
   * Metodo para añadir rango al input
   */
  var rangeApplyEvent = function(elem) {
    $('#' + elem.id).on('apply.daterangepicker', function(event, picker) {
      $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
    });
  };

  /**
   * Metodo para añadir fecha al input
   */
  var singleApplyEvent = function(elem) {
    $('#' + elem.id).on('apply.daterangepicker', function(event, picker) {
      $(this).val(picker.endDate.format('DD/MM/YYYY'));
    });
  };

  /**
   * Metodo para limpiar el rango seleccionado
   */
  var rangeClearEvent = function(elem) {
    $('#' + elem.id).on('cancel.daterangepicker', function(event, picker) {
      $(this).val('');
    });
  };

  /**
   * Metodo para inicializar un rangepicker
   * Recibe un elemento json para la configuracion de propiedades
   */
  var dateRangePicker = function(elem) {
    $('#' + elem.id).daterangepicker({
      autoUpdateInput: elem.autoUpdateInput,
      locale: {
        format: elem.locale.format,
        separator: ' - ',
        applyLabel: elem.locale.applyLabel,
        cancelLabel: elem.locale.cancelLabel,
        fromLabel: elem.locale.fromLabel,
        toLabel: elem.locale.toLabel,
        daysOfWeek: elem.locale.daysOfWeek,
        monthNames: elem.locale.monthNames,
        firstDay: elem.locale.firstDay
      }
    });

    //Se recoge en un array las func a aplicar
    _.forEach(elem.events, function(value) {
      if (value == "rangeApplyEvent") {
        rangeApplyEvent(elem);
      } else if (value == "singleApplyEvent") {
        singleApplyEvent(elem);
      } else {
        rangeClearEvent(elem);
      }
    });
  };

  /**
   * Metodo para inicializar un singlepicker
   * Recibe un elemento json para la configuracion de propiedades
   */
  var singleDatePicker = function(elem) {
    $('#' + elem.id).daterangepicker({
      autoUpdateInput: elem.autoUpdateInput,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        format: elem.locale.format,
        daysOfWeek: elem.locale.daysOfWeek,
        monthNames: elem.locale.monthNames,
        firstDay: elem.locale.firstDay
      }
    });

    // Se recoge en un array las func a aplicar
    _.forEach(elem.events, function(value) {
      if (value == "rangeApplyEvent") {
        rangeApplyEvent(elem);
      } else if (value == "singleApplyEvent") {
        singleApplyEvent(elem);
      } else {
        rangeClearEvent(elem);
      }
    });
  };

  /**
   * Lista de funciones/variables accesibles (publicas)
   */
  return {
    rangeApplyEvent: rangeApplyEvent,
    singleApplyEvent: singleApplyEvent,
    rangeClearEvent: rangeClearEvent,
    dateRangePicker: dateRangePicker,
    singleDatePicker: singleDatePicker
  };
};
