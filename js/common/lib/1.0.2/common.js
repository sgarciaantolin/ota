/**
 * UDA - DonostiaTIK
 * 2018
 * EUROHELP CONSULTING SL
 */

/**
 * Clase SINGLETON para iniciar toda la app
 */
Common = (function() {

  var my = {};

  // **************
  // *** PUBLIC ***
  // **************

  // my.datatableLanguageUrl = "./JS_CSS/datatables_language/Basque.json";
  my.selectedLanguageDataTableDateType = 'date-yyyy-mmm-dd';
  my.selectedLanguageMomentDateType = 'YYYY-MM-DD';
  my.selectedLanguageMomentDateType2 = 'DD/MM/YYYY';
  my.selectedLanguageDateType = 'yyyy-mm-dd';
  my.WSMomentDateType = 'DD/MM/YYYY';
  // my.template_dis = true;
  my.localeIndex = 0;
  my.locale = 'es';

  /**
   * Metodo para inicializar la clase
   */
  my.init = function() {
    setLanguage(my.locale);
  };

  /**
   * Metodo para realizar una llamada AJAX
   */
  my.requestWS = function(obj) {

    // Variables
    var url = obj.url;
    var method = obj.method || "GET";
    var data = obj.data;
    var contentType = obj.contentType || "application/json; charset=utf-8";
    var dataType = obj.dataType || "json";
    var successFn = obj.successFn;
    var errorFn = obj.errorFn;
    var completeFn = obj.completeFn;

    // Construir objecto ajax
    var ajaxObj = {
      url: url,
      type: method,
      data: data, // Datos a enviar
      contentType: contentType, // Tipo de datos a enviar
      dataType: dataType, // Tipo de datos a recibir
      success: function(response) {
        if (typeof(successFn) == 'function') {
          successFn(response);
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        if (jqXHR.status === 0) {
          console.log('requestWS:Error: Not connect: Verify Network.');
        } else if (jqXHR.status == 404) {
          console.log('requestWS:Error: Requested page not found [404]');
        } else if (jqXHR.status == 500) {
          console.log('requestWS:Error: Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
          console.log('requestWS:Error: URL is null or Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
          console.log('requestWS:Error: Time out error.');
        } else if (textStatus === 'abort') {
          console.log('requestWS:Error: Ajax request aborted.');
        } else {
          console.log("requestWS:Error: jqXHR => " + jqXHR.statusText);
          console.log("requestWS:Error: textStatus => " + textStatus);
          console.log("requestWS:Error: errorThrown => " + errorThrown);
        }
        Dialog.alert(messages.error_de_comunicacion_con_servidor);
        if (typeof(errorFn) == 'function') {
          errorFn();
        }
      },
      complete: function(jqXHR, textStatus) {
        if (typeof(completeFn) == 'function') {
          completeFn();
        }
      }
    };

    // Realizar llamada AJAX
    $.ajax(ajaxObj);
  };

  /**
   * Abrir fichero seleccionado
   */
  my.getAssociatedFile = function(url, data) {

    // Mostrar Spinner
    $('#main_catas_sondeos_portlet .spinner-content').show();

    // Callbacks
    var successFn = function(response) {
      if (response.url == null) {
        $("#main_catas_sondeos_portlet #messageError").show();
      } else {
        var openTab = window.open(response.url, '_blank');
        if (openTab) {
          //Browser has allowed it to be opened
          openTab.focus();
        } else {
          //Browser has blocked it
          alert('Por favor, habilite las ventanas emergentes en su navegador.'); //'Por favor, habilite las ventanas emergentes en su navegador.'
        }
      }
    };
    var errorFn = function(err) {
      $("#main_catas_sondeos_portlet #messageError").show();
      console.log("getAssociatedFile:Error:  StatusText => " + err.statusText);
      console.log("getAssociatedFile:Error:  responseText => " + err.responseText);
    };
    var completeFn = function() {
      $('#main_catas_sondeos_portlet .spinner-content').hide();
    };

    // Llamada AJAX
    my.requestWS({
      url: url,
      method: "POST",
      data: data,
      contentType: "application/x-www-form-urlencoded; charset=UTF-8",
      successFn: successFn,
      errorFn: errorFn,
      completeFn: completeFn
    });
  };

  // ***************
  // *** PRIVATE ***
  // ***************

  /**
   * Función llamada desde el aspx para que pinte el idoma seleccionado
   * y cree los eventos click en los idiomas.
   */
  var setLanguage = function(selectedLanguage) {
    SetLanguageTextStyle(selectedLanguage);
  };

  /**
   * Función que pone con estilo activo el idioma seleccionado y
   * desactivados el resto.
   */
  var SetLanguageTextStyle = function(selectedLanguage) {
    // Euskera seleccionado. Activamos Euskera, desactivamos el resto.
    if (selectedLanguage == "eu") {
      //Las fechas de las tablas siempre se muestran en formato español
      my.selectedLanguageDataTableDateType = 'date-dd-mmm-yyyy';
      my.selectedLanguageDateType = 'yyyy-mm-dd';
      my.selectedLanguageMomentDateType = 'YYYY-MM-DD';
      my.localeIndex = 1;
    }
    // Español seleccionado. Activamos Español, desactivamos el resto.
    else if (selectedLanguage == "es") {
      my.selectedLanguageDataTableDateType = 'date-dd-mmm-yyyy';
      my.selectedLanguageDateType = 'dd-mm-yyyy';
      my.selectedLanguageMomentDateType = 'DD-MM-YYYY';
      my.localeIndex = 0;
    }
    my.selectedLanguageMomentDateType2 = 'DD/MM/YYYY';
  };

  // **************
  // *** RETURN ***
  // **************

  /**
   * Lista de funciones y variables publicas
   */
  return my;

})();

/**
 * Ejecutar codigo cuando la pagina este totalmente cargada.
 */
$(document).ready(function() {

  // TODO: Modificar parámetro en producción
  Common.init();

  // Dispara el evento 'domLoaded'
  $(document).trigger("domLoaded");
});
