/**
 * Mensajes en Euskera
 */
var messages = {

  // WORDS
  n: "Nº", // Numero
  i: "I.", // Idiomas
  l: "Ordaindua",
  tel: "Tel.", // Telefono
  fre: "Fre.", // Frecuencia
  club: "Club",
  edad: "Edad",
  hora: "Hora",
  fecha: "Fecha",
  email: "Email",
  lugar: "Lugar",
  curso: "Curso",
  e_min: "E.Min.",
  e_max: "E.Max.",
  f_ini: "F.Ini.",
  f_fin: "F.Fin.",
  estdo: "Estdo.",
  desde: "Desde",
  hasta: "Hasta",
  cancelar: "Cancelar",
  f_curs: "F.Curs.", // Fecha Inscripción
  h_curs: "H.Curs.", // Horario Cursillo
  nombre: "Nombre",
  anular: "Anular",
  saldar: "Saldar",
  idioma: "Idioma",
  estado: "Estado",
  extras: "Extras",
  activa: "Activa",
  pagada: "Pagada",
  suplem: "Suplem.",
  p_base: "P. Base",
  cumple: "Cumple",
  guardar: "Guardar",
  importe: "Importe",
  usuario: "Usuario",
  horario: "Horario",
  anulada: "Anulada",
  euskera: "Euskera",
  p_final: "P. final", // Precio Final
  ins_web: "Ins. Web",
  incumple: "Incumple",
  eliminar: "Eliminar",
  duplicar: "Duplicar",
  borrador: "Borrador",
  pdte_rev: "Pdte. Rev.",
  revisada: "Revisada",
  f_inicio: "F.Inicio",
  agraciada: "Agraciada",
  actividad: "Actividad",
  categoria: "Caegoria",
  requerido: "Requerido",
  rechazada: "Rechazada",
  pendiente: "Pendiente",
  confirmada: "Confirmada",
  frecuencia: "Frecuencia",
  suplemento: "Suplemento",
  nombres_es: "Nombre ES",
  nombres_eu: "Nombre EU",
  responsable: "Responsable",
  cumplimento: "Cumplimento",
  no_agraciada: "No agraciada",
  tipo_actividad: "Tipo Actividad",
  nombre_def_cursillo: "Nombre Def. Cursillo",
  periodo_inscripciones: "Periodo Inscripciones",
  periodo_preinscripciones: "Periodo Preinscripciónes",
  fecha_y_hora_del_informe: "Fecha y Hora del Informe",

  // PHRASES
  fecha_fin: "Fecha fin",
  fecha_hasta: "Fecha hasta",
  fecha_desde: "Fecha desde",
  fecha_inicio: "Fecha inicio",
  enviar_a_ota: "Enviar a ota",
  mas_informacion: "Más información",
  enviar_a_ota_ask: "Se va a proceder a enviar la propuesta a validar por parte del ota. Esto bloqueará la edición de la propuesta. ¿Está seguro de que ha finalizado con la edición de la propuesta?",
  apellidos_nombre: "Apellidos, Nombre",
  procesando_peticion: "Procesando petición...",
  guardar_def_cursillo: "Se va a proceder a guardar la propuesta actual. Esto generará los cursillos pertinentes por lo que el proceso puede tardar. ¿Está seguro de que ha finalizado con la edición de la propuesta?",
  seleccione_una_opcion: "Seleccione una opcion",
  edadValidaParaElCursillo: "El cursillista no entra dentro del rango de edad del cursillo.",
  lugarDeLasSedesNoRepetidas: "No pueden existir dos sedes con el mismo campo 'Lugar'.",

  // ERRORES GENERICOS
  error_de_comunicacion_con_servidor: "Ha ocurrido un error en la comunicación con el servidor.",
  error_horarios_sin_rellenar: "Existen horarios sin rellenar.",

  // SEDES
  sede_nombre: "Nombre sede",
  sede_telefono: "Teléfono de la sede",

  // PROPUESTAS
  propuesta_id: "ID de la propuesta",
  propuesta_rechazar: "Rechazar propuesta",
  propuesta_importada: "Propuesta importada del año anterior",
  propuesta_necesario_marcar_para_importar: "Es necesario marcar alguna propuesta para importar",
  // PROPUESTAS : ASK
  propuesta_anular_ask: "Va a proceder a anular la propuesta {0}. ¿Está seguro?",
  propuesta_duplicar_ask: "Va a proceder a duplicar la propuesta {0}. ¿Está seguro?",
  propuesta_rechazar_ask: "Va a proceder a rechazar la propuesta. ¿Está seguro?",
  // PROPUESTAS : ERROR
  propuesta_anular_error: "<strong>Error!</strong> No se ha podido anular la propuesta.",
  propuesta_duplicar_error: "<strong>Error!</strong> No se ha podido duplicar la propuesta.",
  propuesta_rechazar_error: "<strong>Error!</strong> No se ha podido rechazar la propuesta.",

  // DEFINICION CURSILLO
  def_cursillo_eliminar: "Eliminar Def. Cursillo",
  def_cursillo_eliminar_ask: "Está acción eliminará por completo la definición {0}, la acción no se podrá deshacer. ¿Está seguro de querer eliminarla?",

  // CURSILLO
  cursillo_anular: "Anular cursillo",
  cursillo_nombre: "Nombre del cursillo",
  cursillo_eliminar: "Eliminar cursillo",
  cursillo_duplicar: "Duplicar cursillo",
  cursillo_rectificar: "Rectificar cursillo",
  // CURSILLO: ASK
  cursillo_anular_ask: "Va a proceder a anular el cursillo {0}. ¿Está seguro?",
  cursillo_duplicar_ask: "Va a proceder a duplicar el cursillo {0}. ¿Está seguro?",
  cursillo_eliminar_ask: "Va a proceder a eliminar este cursillo. ¿Está seguro?",
  cursillo_rectificar_ask: "<strong>Los cambios</strong> realizados en los datos del cursillo <strong>pueden afectar a las personas pre-inscritas o inscritas</strong> al mismo. Recuerde que <strong>deberá ponerse en contacto con todas</strong> ellas para informarles de los cambios que les pudieran afectar. <strong>Está seguro de querer continuar?</strong>",
  // CURSILLO: ALERTS
  cursillo_eliminar_inscripciones: "<p>No se puede eliminar el cursillo, todavia quedan inscripciones por eliminar</p>",
  cursillo_eliminar_preinscripciones: "<p>No puede eliminar el cursillo, todavia quedan preinscripciones por eliminar</p>",
  // CURSILLO : ERRROR
  cursillo_anular_error: "<strong>Error!</strong> No se ha podido anular el cursillo.",
  cursillo_duplicar_error: "<strong>Error!</strong> No se ha podido duplicar el cursillo.",

  // INSCRIPCIONES
  inscripcion_f: "F. Inscripción", // Fecha inscripción
  inscripcion_id: "Inscripción ID",
  inscripcion_ver: "Ver inscripción",
  inscripcion_hora: "Hora inscripción",
  inscripcion_online: "Inscripción online",
  inscripcion_anular: "Anular inscripción",
  inscripcion_saldar: "Saldar inscripción",
  inscripcion_rechazar: "Rechazar inscripción",
  inscripcion_en_grupo: "Inscripción en grupo",
  inscripcion_detalles: "Detalles de la inscripción",
  inscripcion_no_online: "No se acepta inscripción online",
  inscripcion_individual: "Inscripción individual",
  inscripcion_presencial: "Inscripción presencial",
  inscripcion_marcar_como_pagada: "Marcar inscripción como pagada",
  inscripcion_cambiar_de_cursillo: "Cambiar inscripción a otro cursillo",
  inscripcion_introduce_fechas_y_horarios: "Introduce las fechas y horarios entre las cuales se podrá hacer la inscripción",
  // INSCRIPCIONES : ASK
  inscripcion_saldar_ask: "Va a proceder a saldar la inscripción de {0}. ¿Está seguro?",
  inscripcion_anular_ask: "Va a proceder a anular la inscripción de {0}. ¿Está seguro?",
  inscripcion_anular_ask_2: "Va a proceder a anular esta inscripción. ¿Está seguro?",
  inscripcion_rechazar_ask: "Va a proceder a rechazar esta inscripción. ¿Está seguro?",
  inscripcion_marcar_como_pagada_ask: "Va a marcar esta inscripción como pagada. ¿Está seguro?",
  // INSCRIPCIONES : ERROR
  inscripcion_saldar_error: "<strong>Error!</strong> No se ha podido saldar la inscripción.",
  inscripcion_anular_error: "<strong>Error!</strong> No se ha podido anular la inscripción.",
  inscripcion_asignar_error: "<strong>Error!</strong> No se ha podido asignar la inscripción.",
  inscripcion_cambiar_error: "<strong>Error!</strong> No se ha podido cambiar la inscripción a otro cursillo.",

  // PREINSCRIPCIONES
  preinscripcion_f: "F. Preinscripción", // Fecha PREINSCRIPCION
  preinscripcion_ver: "Ver preinscripción",
  preinscripcion_hora: "Hora Preinscripción",
  preinscripcion_anular: "Anular Preinscripción",
  preinscripcion_rechazar: "Rechazar Preinscripción",
  preinscripcion_detalles: "Detalles de la preinscripción",
  preinscripcion_en_grupo: "Preinscripción en grupo",
  preinscripcion_individual: "Preinscripción individual",
  preinscripcion_fechas_y_horarios: "Fechas y horarios para preinscripción",
  preinscripcion_marcar_como_pagada: "Marcar preinscripción como pagada",
  preinscripcion_introduce_fechas_y_horarios: "Introduce las fechas y horarios entre las cuales se podrá hacer la preinscripción",
  // PREINSCRIPCIONES : ASK
  preinscripcion_anular_ask: "Va a proceder a anular esta preinscripción. ¿Está seguro?",
  preinscripcion_rechazar_ask: "Va a proceder a rechazar esta preinscripción. ¿Está seguro?",
  preinscripcion_marcar_como_pagada_ask: "Va a marcar esta preinscripción como pagada. ¿Está seguro?",

  // LISTA ESPERA
  espera_asignar: "Asignar inscripción en espera al cursillo",
  // LISTA ESPERA : ASK
  espera_asignar_ask: "Va a proceder a asignar la inscripción de {0}. ¿Está seguro?",
  // LISTA ESPERA : ERROR
  espera_anular_error: "<strong>Error!</strong> No se ha podido anular la inscripción en espera.",
  espera_asignar_error: "<strong>Error!</strong> No se ha podido asignar la inscripción en espera.",

  // CURSILLISTA
  cursillista_alcanzado_el_num_max_de_cursillistas: "Se ha alcanzado el numero maximo de cursillistas ({0}), elimine uno para poder filtrar.",
  // CURSILLISTA : SUCCESS
  cursillista_encontrado_success: "<strong>Exito!</strong> Se han encontrado los datos del cursillista.",
  // CURSILLISTA : WARNING
  cursillista_encontrado_warning: "<strong>Aviso!</strong> No se han encontrado los datos del cursillista.",
  cursillista_ya_existe_un_cursillista_con_esa_identificacion_warning: "<strong>Aviso!</strong> Ya existe un cursillista con esa identificacón.",

  // LIQUIDACIONES
  liquidacion_detalle: "Detalle de la liquidación",

  // Configuracion Campaña ota
  propuestasIni: "Inicio periodo propuestas",
  propuestasFin: "Fin periodo propuestas",
  cursillosIni: "Inicio periodo cursillos",
  cursillosFin: "Fin periodo cursillos",
  preinscripcionIni: "Inicio periodo preinscripcion",
  preinscripcionFin: "Fin periodo preinscripcion",
  fechaSorteo: "Fecha Sorteo",
  pagosIni: "Inicio periodo pagos",
  pagosFin: "Fin periodo pagos",
  fechaPlazasLibres: "Fecha publicacion plazas libres",
  inscripcionIni: "Inicio periodo inscripcion",
  exportar_csv: "Exportar a CSV",
  exportar_csv_preinscripciones: "Se va a generar un fichero con todas las preinscripciones de la campaña actual, esto puede tardar algunas horas.<br><br>Estás seguro de querer continuar?",
  exportar_csv_inscripciones: "Se va a generar un fichero con todas las inscripciones de la campaña actual, esto puede tardar algunas horas.<br><br>Estás seguro de querer continuar?",
  rechazarOk: "Si",
  rechazarCancel: "No",
  rechazarPropuesta: "Rechazar propuesta",
  rechazarPropuestaText: "Si rechaza la propuesta se eliminaran todos los cursillos generados a partir de esta.<br><br>Desea continuar?"
};
