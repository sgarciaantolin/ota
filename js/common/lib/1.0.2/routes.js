/**
 * UDA - DonostiaTIK
 * 2018
 * EUROHELP CONSULTING SL
 */

/**
 * Clase SINGLETON para trabajar con las direcciónes URL
 */
Routes = (function() {

  // Final
  var GULP = 1;
  var JAVA = 2;

  // Variables
  var my = {};
  var entorno = GULP;

  // **************
  // *** PUBLIC ***
  // **************

  /**
   * Metodo para inicializar la clase
   */
  my.init = function() {};

  /**
   * Metodo que obtiene la url de la pagina
   *
   * @param {string} path - path a aplicar a la url del sitio.
   */
  my.url = function(pathJson) {
    if (entorno == GULP) {
      var path = window.location.pathname;
      var aPath = path.trimChar("/").split("/");
      aPath.pop(); // obtiene el nombre del archivo
      aPath.pop();
      var newPath = aPath.join("/");
      newPath = newPath===""? newPath : "/" + newPath;
      return window.location.origin + newPath +  pathJson;
    } else if (entorno == JAVA) {
      return pathJson ? window.location.origin + "/" + my.path(0) + "/rsc" +  pathJson : window.location.origin;
    } else {
      console.log("Routes:url: Error 'entorno' desconocido => " + entorno);
      return window.location.origin;
    }
  };

  /**
   * Metodo que devuelve el path completo de la url, pero si se especifica un index, devuelve el path en concreto.
   *
   * @param {int} index - [indice del array] numero del path deseado.
   *
   * Ejemplo: https://www.google.com/path1/path2/path3
   * my.path()  => /path1/path2/path3
   * my.path(0)	=> path1
   * my.path(2)	=> path3
   */
  my.path = function(index) {
    // Obtener path de la URL
    var path = window.location.pathname;
    // Si NO existe el "index", devolver path completo, sino el indicado.
    if (index === undefined) {
      return path;
    } else {
      // Convertir a array
      var arrayPath = path.trimChar("/").split("/");
      // Return path del index
      return arrayPath[index];
    }
  };

  /**
   * ...
   */
  my.getDetailUrl = function(id) {
    var url = "detail.html?id=idacambiar";
    if (typeof url_global !== 'undefined' && typeof url_global.detail !== 'undefined') {
      url = url_global.detail;
    }
    url = url.replace("idacambiar", id);
    return url;
  };

  /**
   * ...
   */
  my.getExportUrl = function() {
    // var url =
    // "https://172.16.250.128:9080/WAS/CORP/DIOCatasYSondeosWEB/webServiceOOHH.do?generarShapeSeleccion=1";
    var url = "http://172.16.250.128:9080/WAS/CORP/DIOCatasYSondeosWEB/webServiceOOHH.do";
    if (typeof url_global !== 'undefined' && typeof url_global.export !== 'undefined') {
      url = url_global.export;
    }
    return url;
  };

  /**
   * Metodo para obtener el nombre de una pagina
   */
  my.getPageName = function() {
    var path = location.pathname;
    var page = path.split("/").pop();
    var page_name = page.substring(0, page.length - 5);
    return page_name;
  };

  // ***************
  // *** PRIVATE ***
  // ***************

  /**
   * ...
   */
  var getResourcesDir = function() {
    var path = "../";
    if (typeof liferay_portlet_global !== 'undefined') {
      path = themeDisplay.getPathThemeImages();
      path = path.split("/");
      path.splice(-2, 2);
      path = path.join("/");
      path = path + "/" + liferay_portlet_global + "/";
    }
    return path;
  };

  /**
   * Metodo para obetner un directorio
   */
  var getJSDir = function() {
    if (typeof liferay_portlet_global !== 'undefined') {
      return getResourcesDir() + "js/";
    } else {
      var path = location.protocol + "//" + location.host + location.pathname;
      path = path.split("/");
      path.splice(path.length - 2, 2);
      path = path.join("/") + "/js";
      return path;
    }
  };

  // **************
  // *** RETURN ***
  // **************

  /**
   * Lista de funciones y variables publicas
   */
  return my;

})();
