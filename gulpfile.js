// Include gulp
var gulp = require('gulp');
var fs = require('fs');
var path = require('path');

// Include Our Plugins
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
//var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
//var rename = require('gulp-rename');
var fileinclude = require('gulp-file-include');
var i18n = require('gulp-html-i18n');
var clean = require('gulp-clean');
var connect = require('gulp-connect');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var minimist = require('minimist');
var runSequence = require('run-sequence');
var uglifycss = require('gulp-uglifycss');
/*
var knownOptions = {
  string: 'env',
  default: { env: process.env.NODE_ENV || 'dev' }
};

var options = minimist(process.argv.slice(2), knownOptions);


if(options.env === 'prod'){
  dest = 'dist/';
}
else{
  dest = 'build/';
}
*/

var dest = null;

gulp.task('build', function(callback) {
    dest = 'build/';
    runSequence(
        'lint', 'projectMap', 'sass', 'movefolders', 'fileinclude_localize', 'webserver', 'watch',
        function(error) {
            if (error) {
                console.log(error.message);
            } else {
                console.log('Build Init Succesfully');
            }
            callback(error);
        });
});

gulp.task('dist', function(callback) {
    dest = 'dist/';
    runSequence(
        'lint', 'projectMap', 'sass', 'movefolders', 'fileinclude_localize',
        function(error) {
            if (error) {
                console.log(error.message);
            } else {
                console.log('Dist Init Succesfully');
            }
            callback(error);
        });
});

gulp.task('dist_dev', function(callback) {
    dest = 'dist_dev/';
    runSequence(
        'lint', 'projectMap', 'sass', 'movefolders', 'fileinclude_localize',
        function(error) {
            if (error) {
                console.log(error.message);
            } else {
                console.log('Dist Init Succesfully');
            }
            callback(error);
        });
});

// Lint Task
gulp.task('lint', function() {
    return gulp.src(['assets/**/*.js', 'js/**/*.js', '!assets/js/**/data/**/*', '!js/**/data/**/*'])
        .pipe(plumber({
            errorHandler: function(err) {
                notify.onError({
                    title: "Gulp error in " + err.plugin,
                    message: err.toString()
                })(err);
            }
        }))
        .pipe(
          jshint({
            "multistr": true
          })
        )
        .pipe(jshint.reporter('default'));
});

gulp.task('projectMap', function () {
    source = 'www';
    var files = fs.readdirSync(source);


    var indexCode = function(dir, html){

        var content = '<!DOCTYPE html>\
            <html lang="es" class="no-js">\
              <head>\
                <meta charset="UTF-8">\
                <meta http-equiv="cache-control" content="max-age=0">\
                <meta http-equiv="cache-control" content="no-cache">\
                <meta http-equiv="expires" content="0">\
                <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">\
                <meta http-equiv="pragma" content="no-cache">\
                <title>project-map</title>\
                <meta name="description" content="">\
              </head>\
              <body>\
                <ul>';
            content += html
            content +='</ul>\
                <script src="//localhost:35729/livereload.js?snipver=1" async="" defer=""></script></body>\
                </html>';

        fs.writeFileSync(dir + '/index.html', content);
    };


    for (var i = files.length - 1; i >= 0; i--) {
        var dir = path.join(source, files[i]);
        var html = "";
        if(fs.statSync(dir).isDirectory())  {
            //console.log(dir);
            var subfiles = fs.readdirSync(dir);
            //console.log(subfiles);
            for (var j = 0; j < subfiles.length; j++) {
                if(subfiles[j].indexOf('.html') > -1 && subfiles[j] != 'index.html')  {
                    //console.log(subfiles[j]);
                    html  += "<li><a href=\"" + subfiles[j] +"\">" + subfiles[j] + "</a></li>";
                }
            }
            indexCode(dir, html);
        }
    }

});

// Compile Our Sass
gulp.task('sass', function() {
    if (dest === 'build/' || dest === 'dist_dev/') {
        gulp.src('assets/**/*.scss')
            .pipe(plumber({
                errorHandler: function(err) {
                    notify.onError({
                        title: "Gulp error in " + err.plugin,
                        message: err.toString()
                    })(err);
                }
            }))
            .pipe(sass())
            .pipe(gulp.dest(dest + 'assets'));

            gulp.src('styles/**/*.scss')
            .pipe(plumber({
                errorHandler: function(err) {
                    notify.onError({
                        title: "Gulp error in " + err.plugin,
                        message: err.toString()
                    })(err);
                }
            }))
            .pipe(sass())
            .pipe(gulp.dest(dest + 'styles'));

    } else {
        gulp.src('assets/**/*.scss')
            .pipe(plumber({
                errorHandler: function(err) {
                    notify.onError({
                        title: "Gulp error in " + err.plugin,
                        message: err.toString()
                    })(err);
                }
            }))
            .pipe(sass())
            .pipe(uglifycss({
                "maxLineLen": 80,
                "uglyComments": true
            }))
            .pipe(gulp.dest(dest + 'assets'));

            gulp.src('styles/**/*.scss')
            .pipe(plumber({
                errorHandler: function(err) {
                    notify.onError({
                        title: "Gulp error in " + err.plugin,
                        message: err.toString()
                    })(err);
                }
            }))
            .pipe(sass())
            .pipe(uglifycss({
                "maxLineLen": 80,
                "uglyComments": true
            }))
            .pipe(gulp.dest(dest + 'styles'));
    }
});


gulp.task('movefolders', ['movefolders_css', 'movefolders_img', 'movefolders_js',
    'movefolders_vendor'
], function() {});

gulp.task('movefolders_css', function() {
    if (dest === 'build/' || dest === 'dist_dev/') {
        gulp.src(['assets/**/*.css'])
            .pipe(plumber({
                errorHandler: function(err) {
                    notify.onError({
                        title: "Gulp error in " + err.plugin,
                        message: err.toString()
                    })(err);
                }
            }))
            .pipe(gulp.dest(dest + 'assets'));

        gulp.src(['styles/**/*.css'])
            .pipe(plumber({
                errorHandler: function(err) {
                    notify.onError({
                        title: "Gulp error in " + err.plugin,
                        message: err.toString()
                    })(err);
                }
            }))
            .pipe(gulp.dest(dest + 'styles'));
    } else {
        gulp.src(['assets/**/*.css'])
            .pipe(plumber({
                errorHandler: function(err) {
                    notify.onError({
                        title: "Gulp error in " + err.plugin,
                        message: err.toString()
                    })(err);
                }
            }))
            .pipe(uglifycss({
                "maxLineLen": 80,
                "uglyComments": true
            }))
            .pipe(gulp.dest(dest + 'assets'));

        gulp.src(['styles/**/*.css'])
            .pipe(plumber({
                errorHandler: function(err) {
                    notify.onError({
                        title: "Gulp error in " + err.plugin,
                        message: err.toString()
                    })(err);
                }
            }))
            .pipe(uglifycss({
                "maxLineLen": 80,
                "uglyComments": true
            }))
            .pipe(gulp.dest(dest + 'styles'));
    }
});
gulp.task('movefolders_img', function() {
    gulp.src(['assets/**/img/*'])
        .pipe(plumber({
            errorHandler: function(err) {
                notify.onError({
                    title: "Gulp error in " + err.plugin,
                    message: err.toString()
                })(err);
            }
        }))
        .pipe(gulp.dest(dest + 'img'));

        gulp.src(['img/**/*'])
        .pipe(plumber({
            errorHandler: function(err) {
                notify.onError({
                    title: "Gulp error in " + err.plugin,
                    message: err.toString()
                })(err);
            }
        }))
        .pipe(gulp.dest(dest + 'img'));
});
gulp.task('movefolders_js', function() {
    if (dest === 'build/' || dest === 'dist_dev/') {
        gulp.src(['assets/**/*.js', '!assets/**/*.json'])
            .pipe(plumber({
                errorHandler: function(err) {
                    notify.onError({
                        title: "Gulp error in " + err.plugin,
                        message: err.toString()
                    })(err);
                }
            }))
            .pipe(gulp.dest(dest + 'assets'));

        gulp.src(['js/**/*.js', '!js/**/*.json'])
            .pipe(plumber({
                errorHandler: function(err) {
                    notify.onError({
                        title: "Gulp error in " + err.plugin,
                        message: err.toString()
                    })(err);
                }
            }))
            .pipe(gulp.dest(dest + 'js'));
    } else {
        gulp.src(['assets/**/*.js', '!assets/**/*.json'])
            .pipe(plumber({
                errorHandler: function(err) {
                    notify.onError({
                        title: "Gulp error in " + err.plugin,
                        message: err.toString()
                    })(err);
                }
            }))
            .pipe(uglify())
            .pipe(gulp.dest(dest + 'assets'));

            gulp.src(['js/**/*.js', '!js/**/*.json'])
            .pipe(plumber({
                errorHandler: function(err) {
                    notify.onError({
                        title: "Gulp error in " + err.plugin,
                        message: err.toString()
                    })(err);
                }
            }))
            .pipe(uglify())
            .pipe(gulp.dest(dest + 'js'));
    }

    gulp.src(['assets/**/*', '!assets/**/*.js'])
    .pipe(plumber({
        errorHandler: function(err) {
            notify.onError({
                title: "Gulp error in " + err.plugin,
                message: err.toString()
            })(err);
        }
    }))
    .pipe(gulp.dest(dest + 'assets'));

    gulp.src(['js/**/*', '!assets/**/*.js'])
    .pipe(plumber({
        errorHandler: function(err) {
            notify.onError({
                title: "Gulp error in " + err.plugin,
                message: err.toString()
            })(err);
        }
    }))
    .pipe(gulp.dest(dest + 'js'));

});
gulp.task('movefolders_vendor', function() {
    gulp.src(['vendor/**/*'])
        .pipe(plumber({
            errorHandler: function(err) {
                notify.onError({
                    title: "Gulp error in " + err.plugin,
                    message: err.toString()
                })(err);
            }
        }))
        .pipe(gulp.dest(dest + 'vendor'));
});


gulp.task('fileinclude_localize', function() {
    gulp.src(['www/**/*.html'])
        .pipe(plumber({
            errorHandler: function(err) {
                notify.onError({
                    title: "Gulp error in " + err.plugin,
                    message: err.toString()
                })(err);
            }
        }))
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))/*
        .pipe(i18n({
            langDir: 'lang',
            trace: true,
            failOnMissing: true,
            createLangDirs: true
        }))*/
        .pipe(gulp.dest(dest));
});


gulp.task('clean', function() {
    gulp.src(['build/', 'dist/', 'dist_dev/'], { read: false })
        .pipe(plumber({
            errorHandler: function(err) {
                notify.onError({
                    title: "Gulp error in " + err.plugin,
                    message: err.toString()
                })(err);
            }
        }))
        .pipe(clean());
});

gulp.task('webserver', function() {
    connect.server({
        port: 8084,
        root: ['./' + dest],
        livereload: true
    });
});
/*
//livereload
gulp.task('livereload', function() {
  gulp.src(dest)
    .pipe(connect.reload());
});
*/
// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('assets/**/*.js', ['lint']);
    gulp.watch('assets/**/css/**/*', ['movefolders_css']);
    gulp.watch('assets/**/scss/**/*', ['sass']);
    gulp.watch('assets/**/*', ['movefolders_img']);
    gulp.watch('assets/**/*', ['movefolders_js']);
    gulp.watch('vendor/**/*', ['movefolders_vendor']);
    gulp.watch('www/**/*.html', ['fileinclude_localize']);
    //gulp.watch('lang/**/*', ['fileinclude_localize']);
    //gulp.watch(dest, ['livereload']);
});

// Default Task
//gulp.task('build', ['lint', 'movefolders', 'fileinclude_localize', 'watch']);
//gulp.task('dist', ['lint','movefolders', 'fileinclude_localize']);



/*
// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src('assets/*.js')
        .pipe(concat('all.js'))
        .pipe(gulp.dest('dist'))
        .pipe(rename('all.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist/assets'));
});
*/
