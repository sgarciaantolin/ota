/**
 * UDA - DonostiaTIK
 * 2018
 * EUROHELP CONSULTING SL
 */

/**
 * Clase SINGLETON para metodos genericos y utiles.
 */
Utils = (function() {

  var my = {};

  /**
   * Añadir al prototype de String la función 'format'.
   */
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, index) {
      return (typeof args[index] != 'undefined') ? args[index] : match;
    });
  };

  /**
   * Añadir al prototype de String la función 'trimChar'.
   */
  String.prototype.trimChar = function(char) {
    char = "\\" + char;
    return this.replace(new RegExp("^[" + char + "]+|[" + char + "]+$", "g"), "");
  };

  /**
   * Añadir al prototype de jQuery la función 'serializeObject'.
   */
  $.fn.serializeObject = function() {

    var map = {};
    var data = this.serializeArray();

    $.each(data, function() {

      // FIXME: Quitar dependencia con 'config'
      var value;
      // var date = moment(this.value, config.dateFormat, true);
      var date = moment(this.value, config.dateFormat, true);
      var time = moment(this.value, config.timeFormat, true);

      // Si es una fecha, asignar fecha.
      if (date.isValid()) {
        value = date.format();
      }
      // Si es una hora, asignar hora.
      else if (time.isValid()) {
        value = time.format("HH:mm");
      }
      // Si no, asignar valor por defecto.
      else {
        value = this.value;
      }

      // Rellenar el Map
      if (map[this.name]) {
        if (!map[this.name].push) {
          map[this.name] = [map[this.name]];
        }
        map[this.name].push(value || '');
      } else {
        map[this.name] = value || '';
      }

    });

    // Return
    return map;
  };

  // **************
  // *** PUBLIC ***
  // **************

  /**
   * Comprueba si una variable es numerica indistintamente de su tipo
   */
  my.isNumber = function(x) {
    return !isNaN(x);
  };

  /**
   * Comprueba si una variable es un numero entero
   */
  my.isInteger = function(x) {
    return x % 1 === 0;
  };

  /**
   * Comprueba si una variable es un numero decimal
   */
  my.isDouble = function(x) {
    return x % 1 !== 0;
  };

  /**
   * Metodo codificar en base64
   */
  my.encodeId = function(id) {
    return btoa(id);
  };

  /**
   * Metodo decodificar en base64
   */
  my.decodeId = function(id) {
    return atob(id);
  };

  /**
   * Comprueba una variable de la siguiente forma:
   *
   * - No sea undefined.
   * - No sea null.
   * - Si es numero, que no sea 0.
   * - Si es String, que no este vacio.
   * - Si es un array, que no este vacio.
   * - Si es un mapa, que no este vacio.
   */
  my.isset = function(value) {

    if (typeof(value) == "string") {
      value.trim();
    }
    if ((value === undefined) || (value === null) || (value === false) || (value === []) || (value === {}) || (value === "") || (value === 0)) {
      return false;
    }
    return true;
  };

  /**
   * Metodo para comprobar si un array tiene algun item duplicado.
   */
  my.hasDuplicates = function(array) {
    var values = [];
    for (var i = 0; i < array.length; i++) {
      var value = array[i];
      if (!values.indexOf(value)) {
        return true;
      }
      values.push(value);
    }
    return false;
  };

  /**
   * Formatea una plantilla con los datos pasados como parametros
   */
  my.useTemplate = function(template, data) {

    // Comprobar datos
    if (!my.isset(template) || !my.isset(data)) {
      return;
    }

    // Variables
    var regex, html = template;

    // Replace all atributes {{XXX}} with the corresponding property
    for (var key in data) {
      regex = new RegExp('{{' + key + '}}', 'ig');
      html = html.replace(regex, data[key]);
    }

    // Give back the HTML to be added to the DOM
    return html;
  };

  // **************
  // *** RETURN ***
  // **************

  /**
   * Lista de funciones/variables accesibles (publicas)
   */
  return my;

})();
