/**
 * UDA - DonostiaTIK
 * 2018
 * EUROHELP CONSULTING SL
 */

/**
 * Clase SINGLETON para la creación de notificaciones
 *
 * TODO: Añadir posiblidad de tener mas de un <div id="notify_content"> por pagina.
 */
Notify = (function() {

  var my = {};

  // **************
  // *** PUBLIC ***
  // **************

  // TYPES
  my.INFO     = "alert-info";
  my.SUCCESS  = "alert-success";
  my.WARNING  = "alert-warning";
  my.ERROR    = "alert-error";

  /**
   * Metodo que añade y muestra una notificación en un div con ID 'notify_content'.
   *
   * @param data.type: Enum, Tipo de notificación
   * @param data.msg: String, Mensaje de la notificación
   * @param data.autohide: int, Tiempo en milisegundos para mostrar la notificación y luego eliminarla, 0 = indefinido.
   * @param data.close: boolean, Habilitar/Deshabilitar botón para cerrar la notiificación
   */
  my.show = function(data) {
    // Añadir boton para cerrar la notificación
    var btnClose = data.close ? '<button type="button" class="close" data-dismiss="alert">&times;</button>' : '';
    // Notify
    var notify = '<div style="display: none;" id="notify" class="alert ' + data.type + '" data-dismiss="alert">' +
                    btnClose +
                    data.msg +
                  '</div>';
    // Insertar y mostrar notificación en el DIV con ID 'notify_content'
    $(notify).appendTo($('#notify_content')).slideDown();
    // Si existe tiempo, cerrar notificacion despues de ese tiempo
    if (data.autohide) {
      $('#notify').delay(data.autohide).slideUp(function() {
        $(this).alert('close');
      });
    }
    // Eliminar notificacion una vez cerrada.
    $('#notify').on('close.bs.alert', function(event) {
      this.remove();
    });
  };

  // **************
  // *** RETURN ***
  // **************

  /**
   * Lista de funciones y variables publicas
   */
  return my;

})();
